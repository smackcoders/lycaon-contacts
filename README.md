﻿Installation: Famous few minutes install

1. Unzip the package in an empty directory and upload everything.
2. Set the write mode for follwing files assets, runtime under protected, config.php under protected/config.
3. Open install/install.php in your browser.give your configuration details.it will take few minutes to make installation.
4. Once the configuration file is set up, the installer will set up the tables needed for your blog. If there is an error, double check your config.php file, and try again. If it fails again, please go to the support forums with as much data as you can gather.
5. The installer should then send you to the login page.
6. Sign in with the username and password you choose during the installation. If a password was generated for you, you can then click on 'Profile' to change the password. 
