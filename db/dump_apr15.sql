-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: newsletter
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_invitedusers`
--

DROP TABLE IF EXISTS `tbl_invitedusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invitedusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviter_id` int(11) NOT NULL,
  `email_service` varchar(20) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `subscription` tinyint(1) DEFAULT '1',
  `subscription_key` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_contact_id` (`contact_id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invitedusers`
--

LOCK TABLES `tbl_invitedusers` WRITE;
/*!40000 ALTER TABLE `tbl_invitedusers` DISABLE KEYS */;
INSERT INTO `tbl_invitedusers` VALUES (1,14,NULL,1,0,1,'555baab124b113e86cc309b0ffc641cc'),(4,19,NULL,4,0,1,'0176fbc9694a8f3a7a28ed4a6590c0a4'),(5,19,'gmail',5,0,1,'d13a352dc5366a99d91a61c5a1b7165d'),(6,19,'gmail',6,0,1,'cd0e19cdf8a65ba9163fcaedaa167d72'),(7,19,'gmail',7,0,1,'0a5add3f6ec20fe404f890d546de10a6'),(8,19,'gmail',8,0,1,'2df17c11c539abe0095fc2dec7c81e3e'),(9,19,'gmail',9,0,1,'cba37ba6120a0866a1ef0e362cdc5ca0'),(10,19,'gmail',10,0,1,'448f9395c13b990b6c81ea2b3b0ac18d'),(11,19,'gmail',3,0,1,'48adb28e9c63d1a32aeedfc6912c359a'),(12,19,'gmail',11,0,1,'64edd63cd39075bde37a68be2b2ba8a4'),(13,19,'gmail',12,0,0,'50cd249a6cbb7c53e988ff18b55861fc'),(14,14,'gmail',11,0,1,'3f243ec63c1dcc7a77427ab750723695'),(15,14,'gmail',5,0,1,'5ea2ba47a345c784e5776d1da6db8fc6'),(16,14,'gmail',12,0,1,'440505ff4dd158acbf7325478e6a9713'),(17,14,'gmail',10,0,1,'5a75f03eac2be8d14d8ce4a2ecf66261'),(18,14,'gmail',9,0,1,'922603c69134eebcde73f365c9e72d22'),(19,14,'gmail',6,0,1,'5eb1eda97f968720f34e99ebabb80beb'),(20,14,'gmail',4,0,1,'014ee1e1ef6c46deda05e34c3aeb5ce8'),(21,14,'gmail',3,0,1,'f50bd47f03ac16ef74a1eb6584a8569c'),(22,14,'gmail',13,0,1,'d9ed0510a795e93c9314a15a927432a2');
/*!40000 ALTER TABLE `tbl_invitedusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invites`
--

DROP TABLE IF EXISTS `tbl_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invites` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `work_phone` varchar(20) DEFAULT NULL,
  `home` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `company` varchar(125) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invites`
--

LOCK TABLES `tbl_invites` WRITE;
/*!40000 ALTER TABLE `tbl_invites` DISABLE KEYS */;
INSERT INTO `tbl_invites` VALUES (1,'fredrick','sujindoss',NULL,'fredrick sujindoss','04624000004','04622533539','9445344930','2555336','fredrick.sujin@gmail.com','smackcoders',1,0),(3,'rajkumar','',NULL,'rajkumar ','','','','','rajkumarm@smackcoders.com','',1,0),(4,'','','',' ','','','','','muthukumarp@smackcoders.com','',1,0),(5,NULL,NULL,NULL,'premkumarr@smackcoders.com',NULL,NULL,NULL,NULL,'premkumarr@smackcoders.com',NULL,1,0),(6,NULL,NULL,NULL,'Fenzik Joseph',NULL,NULL,NULL,NULL,'fenzik@smackcoders.com',NULL,1,0),(7,NULL,NULL,NULL,'Rajue',NULL,NULL,NULL,NULL,'mailtest@smackcoders.com',NULL,1,0),(8,NULL,NULL,NULL,'Muthu Kumar',NULL,NULL,NULL,NULL,'pearl13lover@gmail.com',NULL,1,0),(9,NULL,NULL,NULL,'Arul Manickam',NULL,NULL,NULL,NULL,'arulmanickams@smackcoders.com',NULL,1,0),(10,NULL,NULL,NULL,'Rajue Murugiah',NULL,NULL,NULL,NULL,'rajue@smackcoders.com',NULL,1,0),(11,NULL,NULL,NULL,'Joseph Asir Raja',NULL,NULL,NULL,NULL,'josephs@smackcoders.com',NULL,1,0),(12,NULL,NULL,NULL,'Fredrick Sujindoss',NULL,NULL,NULL,NULL,'fredrickm@smackcoders.com',NULL,1,0),(13,NULL,NULL,NULL,'Arumugam',NULL,NULL,NULL,NULL,'arumozart@gmail.com',NULL,1,0);
/*!40000 ALTER TABLE `tbl_invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_list_contacts`
--

DROP TABLE IF EXISTS `tbl_list_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_list_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `tbl_list_contacts_fk_list_id` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_list_contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_list_contacts`
--

LOCK TABLES `tbl_list_contacts` WRITE;
/*!40000 ALTER TABLE `tbl_list_contacts` DISABLE KEYS */;
INSERT INTO `tbl_list_contacts` VALUES (12,1,3,14),(13,1,4,14),(14,1,5,14),(15,1,9,14),(16,1,12,14),(17,1,1,14),(18,2,12,14),(19,3,3,14),(20,3,4,14),(21,3,5,14),(22,3,6,14),(23,3,9,14),(24,3,10,14),(25,3,11,14),(26,3,12,14),(27,4,3,14),(28,4,4,14),(29,4,5,14),(30,5,9,14),(31,5,11,14),(32,5,12,14);
/*!40000 ALTER TABLE `tbl_list_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lists`
--

DROP TABLE IF EXISTS `tbl_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lists` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_time` int(10) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `fk_owner_id` (`owner_id`),
  CONSTRAINT `fk_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lists`
--

LOCK TABLES `tbl_lists` WRITE;
/*!40000 ALTER TABLE `tbl_lists` DISABLE KEYS */;
INSERT INTO `tbl_lists` VALUES (1,'smaple','description sample',1334414396,14),(2,'group','description',1334426799,14),(3,'smackers','description',1334429525,14),(4,'group1','description',1334429543,14),(5,'sample1','officiallist',1334429561,14);
/*!40000 ALTER TABLE `tbl_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `from_name` varchar(125) NOT NULL,
  `from_email` varchar(125) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `message` longtext,
  `template_id` int(11) DEFAULT NULL,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `template_id` (`template_id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `tbl_newsletter_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter`
--

LOCK TABLES `tbl_newsletter` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles`
--

DROP TABLE IF EXISTS `tbl_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) DEFAULT '',
  `firstname` varchar(50) DEFAULT '',
  `birthday` date DEFAULT '0000-00-00',
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `fk_profiels_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles`
--

LOCK TABLES `tbl_profiles` WRITE;
/*!40000 ALTER TABLE `tbl_profiles` DISABLE KEYS */;
INSERT INTO `tbl_profiles` VALUES (13,'jasper','rajkumar','2012-04-04'),(14,'sujindoss','fredrick','1989-05-01'),(18,'saa','rajue','2003-06-13'),(19,'','muthu','0000-00-00');
/*!40000 ALTER TABLE `tbl_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles_fields`
--

DROP TABLE IF EXISTS `tbl_profiles_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles_fields`
--

LOCK TABLES `tbl_profiles_fields` WRITE;
/*!40000 ALTER TABLE `tbl_profiles_fields` DISABLE KEYS */;
INSERT INTO `tbl_profiles_fields` VALUES (1,'lastname','Last Name','VARCHAR',50,3,1,'','','Incorrect Last Name (length between 3 and 50 characters).','','','','',1,3),(2,'firstname','First Name','VARCHAR',50,3,1,'','','Incorrect First Name (length between 3 and 50 characters).','','','','',0,3),(3,'birthday','Birthday','DATE',0,0,2,'','','','','0000-00-00','UWjuidate','{\"ui-theme\":\"redmond\"}',3,2);
/*!40000 ALTER TABLE `tbl_profiles_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reports`
--

DROP TABLE IF EXISTS `tbl_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_name` varchar(125) NOT NULL,
  `sender_mail` varchar(125) NOT NULL,
  `newsletter` longtext,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `receiver_mail` varchar(125) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `status_key` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reports`
--

LOCK TABLES `tbl_reports` WRITE;
/*!40000 ALTER TABLE `tbl_reports` DISABLE KEYS */;
INSERT INTO `tbl_reports` VALUES (1,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','rajkumarm@smackcoders.com',0,'b53960746e215b29b116fcfe5b925d39'),(2,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','muthukumarp@smackcoders.com',0,'5f95c6fa704b97582eb2886fa5d038b3'),(3,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','premkumarr@smackcoders.com',0,'9b4d3857e4ab6846e0fbd4af6b2f7da7'),(4,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','arulmanickams@smackcoders.com',0,'c0bc92ff68a20ecb26826ac4e6d02e3e'),(5,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','fredrickm@smackcoders.com',0,'c6276f1df180174892990c2e91c0100c'),(6,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','fredrick.sujin@gmail.com',0,'d4aa98414e8c95d7758c4c65fcf187f3'),(7,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:26am','fredrickm@smackcoders.com',0,'3819a8458cc307482131af590983bc28'),(8,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','rajkumarm@smackcoders.com',0,'24012c1b1813c072143c3f3f359442eb'),(9,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','muthukumarp@smackcoders.com',0,'186592f2d5c1a258baf0637132206d16'),(10,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','premkumarr@smackcoders.com',0,'67e2d465baf8d686c59ebb476b069a06'),(11,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','fenzik@smackcoders.com',0,'1b57a290b1c97acd4180dbbf73de9ef4'),(12,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','arulmanickams@smackcoders.com',0,'ec509d348e7fe07b23b587b7ec4dafcc'),(13,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','rajue@smackcoders.com',0,'c8a76964919271fd22deeb19272887bb'),(14,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','josephs@smackcoders.com',0,'ca369cea12fbb99d41211eb19d748533'),(15,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','fredrickm@smackcoders.com',0,'9858e2e0b799ee73764266dd3517b30a'),(16,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','rajkumarm@smackcoders.com',0,'7d1aefdfd034f7a7e1f5bdb379ca4b74'),(17,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','muthukumarp@smackcoders.com',0,'7757f057be31dfc3fc98452883cc472d'),(18,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','premkumarr@smackcoders.com',0,'f63d200b48ebb62cbefe2997fae7c3d5'),(19,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','arulmanickams@smackcoders.com',0,'64a482812933d8f541dad2d4d3105976'),(20,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','josephs@smackcoders.com',0,'27dc74b185c85231f870c4adab6b6f65'),(21,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','','Apr,15,12:27am','fredrickm@smackcoders.com',0,'9d5b251f69f2dcf1751e33b7d68abc4a'),(22,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','rajkumarm@smackcoders.com',0,'53c82ae424fec8cc3e88baeecd0ffa9d'),(23,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','muthukumarp@smackcoders.com',0,'946161f1122fbd2892b8c84fe1c1e690'),(24,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','premkumarr@smackcoders.com',0,'ef80518a076ba90d1a9ccd222da45bbb'),(25,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','arulmanickams@smackcoders.com',0,'beddcf56d03bd2a8160453557a90e11f'),(26,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','fredrickm@smackcoders.com',0,'d11fbf05f59928954eb49bd309313f78'),(27,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','fredrick.sujin@gmail.com',0,'0818715946c570e35313700a61ed60d6'),(28,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','fredrickm@smackcoders.com',0,'734e38863716e8afd2698688a7eeaf37'),(29,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:27am','rajkumarm@smackcoders.com',0,'7e2b79245261834b365c14955d7e9edb'),(30,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','muthukumarp@smackcoders.com',0,'935a64225ed77f13d5d983de5990882c'),(31,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','premkumarr@smackcoders.com',0,'261cd482d320e67ae9b82fa355eb0840'),(32,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','fenzik@smackcoders.com',0,'af59dce1cb587150b558f9c1304f0ee0'),(33,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','arulmanickams@smackcoders.com',0,'2299f40a1570c7829ab55c96cfac3515'),(34,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','rajue@smackcoders.com',0,'38b6055123100fe64d8ef5230ae5e4c6'),(35,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','josephs@smackcoders.com',0,'7ac05f818f19c597f3954fb254eb9f2e'),(36,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','fredrickm@smackcoders.com',0,'47bd1365bd886318495dcb26e82e8c2a'),(37,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','rajkumarm@smackcoders.com',0,'e6da63045b1df53bbb9aa407d608b97e'),(38,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','muthukumarp@smackcoders.com',0,'b538ca33ee3ebd2cb234de40fee7200b'),(39,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','premkumarr@smackcoders.com',0,'d876b02a6cc86c43fdf3b2ebac35b58a'),(40,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','arulmanickams@smackcoders.com',0,'0cd1c443fb4c4c6fc4d0505b4ae80a52'),(41,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','josephs@smackcoders.com',0,'0a33bf00cc08c2fe5b5a59b333ac5cc4'),(42,'Greetings!',14,'Fredrick','fredrickm@smackcoders.com','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n','Apr,15,12:28am','fredrickm@smackcoders.com',0,'7bd2020ef855c0c4df2ad165f6e54583');
/*!40000 ALTER TABLE `tbl_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `fk_template` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_template`
--

LOCK TABLES `tbl_template` WRITE;
/*!40000 ALTER TABLE `tbl_template` DISABLE KEYS */;
INSERT INTO `tbl_template` VALUES (4,'Test1','&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;title&gt;Template&lt;/title&gt;\r\n\r\n&lt;/head&gt;\r\n&lt;body&gt;[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Silver Service&quot; checked=&quot;checked&quot; onclick=&quot;Test(this);&quot;/&gt;\r\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Gold Service&quot;  onclick=&quot;Test(this);&quot;/&gt;\r\n\r\n&lt;div id=&quot;courier&quot;&gt;Select your courier:&lt;br /&gt;\r\n                    Blah blah....Dropdown to come here&lt;/div&gt;\r\n&lt;/p&gt;\r\n                 \r\n&lt;/body&gt;\r\n&lt;/html&gt;\r\n',13),(5,'kumar','&lt;html&gt;\r\n&lt;head&gt;\r\n&lt;title&gt;Template&lt;/title&gt;\r\n&lt;script language=&quot;JavaScript&quot; type=&quot;text/javascript&quot;&gt;\r\n/*&lt;![CDATA[*/\r\nfunction Test(rad){\r\n var rads=document.getElementsByName(rad.name);\r\n document.getElementById(\'courier\').style.display=(rads[1].checked)?\'none\':\'block\';\r\n document.getElementById(\'courier\').style.display=(rads[2].checked)?\'block\':\'none\';\r\n}\r\n/*]]&gt;*/\r\n&lt;/script&gt;\r\n&lt;/head&gt;\r\n&lt;body&gt;[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Silver Service&quot; checked=&quot;checked&quot; onclick=&quot;Test(this);&quot;/&gt;\r\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Gold Service&quot;  onclick=&quot;Test(this);&quot;/&gt;\r\n\r\n&lt;div id=&quot;courier&quot;&gt;Select your courier:&lt;br /&gt;\r\n                    Blah blah....Dropdown to come here&lt;/div&gt;\r\n&lt;/p&gt;\r\n                 \r\n&lt;/body&gt;\r\n&lt;/html&gt;\r\n',13),(8,'Test','\r\n&lt;title&gt;Template&lt;/title&gt;&amp;nbsp;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;br&gt;&lt;p&gt;&lt;/p&gt;\r\n                 \r\n\r\n\r\n',14),(9,'Test2','&lt;html&gt;\n&lt;head&gt;\n&lt;title&gt;Template&lt;/title&gt;\n&lt;script language=&quot;JavaScript&quot; type=&quot;text/javascript&quot;&gt;\n/*&lt;![CDATA[*/\nfunction Test(rad){\n var rads=document.getElementsByName(rad.name);\n document.getElementById(\'courier\').style.display=(rads[1].checked)?\'none\':\'block\';\n document.getElementById(\'courier\').style.display=(rads[2].checked)?\'block\':\'none\';\n}\n/*]]&gt;*/\n&lt;/script&gt;\n&lt;/head&gt;\n&lt;body&gt;[CONTENT]\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\n			You Got a new mail!.\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Silver Service&quot; checked=&quot;checked&quot; onclick=&quot;Test(this);&quot;/&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Gold Service&quot;  onclick=&quot;Test(this);&quot;/&gt;\n\n&lt;div id=&quot;courier&quot;&gt;Select your courier:&lt;br /&gt;\n                    Blah blah....Dropdown to come here&lt;/div&gt;\n&lt;/p&gt;\n                 \n&lt;/body&gt;\n&lt;/html&gt;\n',14),(10,'sample','&lt;html&gt;\n&lt;head&gt;\n&lt;title&gt;Template&lt;/title&gt;\n&lt;script language=&quot;JavaScript&quot; type=&quot;text/javascript&quot;&gt;\n/*&lt;![CDATA[*/\nfunction Test(rad){\n var rads=document.getElementsByName(rad.name);\n document.getElementById(\'courier\').style.display=(rads[1].checked)?\'none\':\'block\';\n document.getElementById(\'courier\').style.display=(rads[2].checked)?\'block\':\'none\';\n}\n/*]]&gt;*/\n&lt;/script&gt;\n&lt;/head&gt;\n&lt;body&gt;[CONTENT]\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\n			You Got a new mail!.\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Silver Service&quot; checked=&quot;checked&quot; onclick=&quot;Test(this);&quot;/&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Gold Service&quot;  onclick=&quot;Test(this);&quot;/&gt;\n\n&lt;div id=&quot;courier&quot;&gt;Select your courier:&lt;br /&gt;\n                    Blah blah....Dropdown to come here&lt;/div&gt;\n&lt;/p&gt;\n                 \n&lt;/body&gt;\n&lt;/html&gt;\n',14),(12,'sample12','&lt;html&gt;\n&lt;head&gt;\n&lt;title&gt;Template&lt;/title&gt;\n&lt;script language=&quot;JavaScript&quot; type=&quot;text/javascript&quot;&gt;\n/*&lt;![CDATA[*/\nfunction Test(rad){\n var rads=document.getElementsByName(rad.name);\n document.getElementById(\'courier\').style.display=(rads[1].checked)?\'none\':\'block\';\n document.getElementById(\'courier\').style.display=(rads[2].checked)?\'block\':\'none\';\n}\n/*]]&gt;*/\n&lt;/script&gt;\n&lt;/head&gt;\n&lt;body&gt;[CONTENT]\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\n			You Got a new mail!.\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Silver Service&quot; checked=&quot;checked&quot; onclick=&quot;Test(this);&quot;/&gt;\n&lt;input name=&quot;selection&quot; type=&quot;radio&quot; value=&quot;Gold Service&quot;  onclick=&quot;Test(this);&quot;/&gt;\n\n&lt;div id=&quot;courier&quot;&gt;Select your courier:&lt;br /&gt;\n                    Blah blah....Dropdown to come here&lt;/div&gt;\n&lt;/p&gt;\n                 \n&lt;/body&gt;\n&lt;/html&gt;\n',14);
/*!40000 ALTER TABLE `tbl_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `org_name` varchar(75) DEFAULT NULL,
  `org_desc` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','webmaster@example.com','','','','','','0',NULL,'9a24eff8c15a6a141ece27eb6947da0f',1261146094,0,1,1),(2,'demo','fe01ce2a7fbac8fafaed7c982a04e229','demo@example.com','','','','','','0',NULL,'099f825543f7850cc038b90aaff39fac',1261146096,0,0,1),(8,'arul','be3025c4341e340ee641e89779ca2649','arulmanickams@smackcoders.com','smackcoders','it','tvl','tamil nadu','india','9003777515','manickamda','9907b2497db4da1ae2d261a856fd001d',1332597314,1333554003,0,1),(9,'rajue','031700427b70a87b0203a7d78a85c0da','rajue@smackcoders.com','smack','smack','chennai','tn','india','9865354604','elinkbox.in','e77d01a9b4f2fbb294b97da0f26a2172',1332845784,1332845922,0,1),(10,'sample','5e8ff9bf55ba3508199d22e984129be6','premkumarr@smackcoders.com','smackcoders','IT Company','Trunelveli','Tamilnadu','India','9999999999','','571f5323340a0e8a5c9d8dab05c4a322',1332941001,0,0,0),(13,'rajkumar','79cfac6387e0d582f83a29a04d0bcdc4','rajkumarm@smackcoders.com','smackcoder','IT','Thirunelveli','TamilNadu','India','9629211536','qwerty','7e63a0d982a45f6691eaaf6aeee32002',1334081949,1334245645,0,1),(14,'sujin','55d674f40a42cb80806e91ad42699311','fredrickm@smackcoders.com','smackcoders','IT','Tirunelveli','Tamilnadu','India','9629211536','www.fredrick.blogspot.com','1866abed80b590541a0b28393abf77ed',1334082988,1334429801,0,1),(18,'rajue1','9cf9ee00ea6d6e448e6b176fe1ba715d','mhurgaiya@gmail.com','pixi','PIXI','chennai','tn','india','9865354604','','95dc63d513efb9c1e13be5ed733b627e',1334159367,1334159653,0,1),(19,'muthu','79cfac6387e0d582f83a29a04d0bcdc4','muthukumarp@smackcoders.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'433489a11c6d54f7e1160b7b6bebb2cd',1334159571,1334332327,0,1);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger insert_in_tbl_users_profiles after insert on
tbl_users for each row
begin
insert into tbl_profiles (user_id,firstname)values(new.id,new.username);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger eliniminate_tbl_users_profiles after delete on
tbl_users for each row 
begin
delete from tbl_profiles where user_id = old.id;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `content` varchar(55540) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES ('&lt;html&gt;\n&lt;head&gt;\n&lt;style type=&quot;text/css&quot;&gt;\np{\ncolor:Black;\nfont-size:20px;\n}\nspan{\ncolor:red;\n}\nbody{\n border:1px solid #6699ff;\nwidth:250px;\nheight:150px;	\n}\n\n&lt;/style&gt;\n&lt;/head&gt;\n&lt;body&gt;\n&lt;a href=\'www.google.com\'&gt;&lt;img src=\'image.jpeg\' , alt =\'SmackCoders\'&gt;&lt;/a&gt;&lt;br /&gt;\n&lt;p&gt;Thanks for registering at &lt;span&gt;Smackers&lt;/span&gt; &lt;br /&gt;You need to verify your email address before you can start using our service.&lt;/p&gt;&lt;br /&gt;\n&lt;p&gt;Please click on the link below in order to verify your email address and activate your account at Smacker Protocal&lt;/p&gt;&lt;br /&gt;\n\n&lt;/body&gt;\n&lt;/html&gt;\n','text');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-15  1:02:10
