-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2012 at 07:52 AM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_newsletter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invitedusers`
--

DROP TABLE IF EXISTS `tbl_invitedusers`;
CREATE TABLE IF NOT EXISTS `tbl_invitedusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviter_id` int(11) NOT NULL,
  `email_service` varchar(20) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `subscription` tinyint(1) DEFAULT '1',
  `subscription_key` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_contact_id` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_invites`
--

DROP TABLE IF EXISTS `tbl_invites`;
CREATE TABLE IF NOT EXISTS `tbl_invites` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `work_phone` varchar(20) DEFAULT NULL,
  `home` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `company` varchar(125) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_lists`
--

DROP TABLE IF EXISTS `tbl_lists`;
CREATE TABLE IF NOT EXISTS `tbl_lists` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_time` int(10) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `fk_owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_list_contacts`
--

DROP TABLE IF EXISTS `tbl_list_contacts`;
CREATE TABLE IF NOT EXISTS `tbl_list_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
CREATE TABLE IF NOT EXISTS `tbl_newsletter` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `from_name` varchar(125) NOT NULL,
  `from_email` varchar(125) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `message` longtext,
  `template_id` int(11) DEFAULT NULL,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `template_id` (`template_id`),
  KEY `list_id` (`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

DROP TABLE IF EXISTS `tbl_profiles`;
CREATE TABLE IF NOT EXISTS `tbl_profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) DEFAULT '',
  `firstname` varchar(50) DEFAULT '',
  `birthday` date DEFAULT '0000-00-00',
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles_fields`
--

DROP TABLE IF EXISTS `tbl_profiles_fields`;
CREATE TABLE IF NOT EXISTS `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_profiles_fields`
--

INSERT INTO `tbl_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3),
(3, 'birthday', 'Birthday', 'DATE', 0, 0, 2, '', '', '', '', '0000-00-00', 'UWjuidate', '{"ui-theme":"redmond"}', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reports`
--

DROP TABLE IF EXISTS `tbl_reports`;
CREATE TABLE IF NOT EXISTS `tbl_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_name` varchar(125) NOT NULL,
  `sender_mail` varchar(125) NOT NULL,
  `newsletter` longtext,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `receiver_mail` varchar(125) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `status_key` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
CREATE TABLE IF NOT EXISTS `tbl_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `org_name` varchar(75) DEFAULT NULL,
  `org_desc` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Triggers `tbl_users`
--
DROP TRIGGER IF EXISTS `insert_in_tbl_users_profiles`;
DELIMITER //
CREATE TRIGGER `insert_in_tbl_users_profiles` AFTER INSERT ON `tbl_users`
 FOR EACH ROW begin
insert into tbl_profiles (user_id,firstname)values(new.id,new.username);
end
//
DELIMITER ;
DROP TRIGGER IF EXISTS `eliniminate_tbl_users_profiles`;
DELIMITER //
CREATE TRIGGER `eliniminate_tbl_users_profiles` AFTER DELETE ON `tbl_users`
 FOR EACH ROW begin
delete from tbl_profiles where user_id = old.id;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
CREATE TABLE IF NOT EXISTS `template` (
  `content` varchar(55540) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`content`, `description`) VALUES
('&lt;html&gt;\n&lt;head&gt;\n&lt;style type=&quot;text/css&quot;&gt;\np{\ncolor:Black;\nfont-size:20px;\n}\nspan{\ncolor:red;\n}\nbody{\n border:1px solid #6699ff;\nwidth:250px;\nheight:150px;	\n}\n\n&lt;/style&gt;\n&lt;/head&gt;\n&lt;body&gt;\n&lt;a href=''www.google.com''&gt;&lt;img src=''image.jpeg'' , alt =''SmackCoders''&gt;&lt;/a&gt;&lt;br /&gt;\n&lt;p&gt;Thanks for registering at &lt;span&gt;Smackers&lt;/span&gt; &lt;br /&gt;You need to verify your email address before you can start using our service.&lt;/p&gt;&lt;br /&gt;\n&lt;p&gt;Please click on the link below in order to verify your email address and activate your account at Smacker Protocal&lt;/p&gt;&lt;br /&gt;\n\n&lt;/body&gt;\n&lt;/html&gt;\n', 'text');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_invitedusers`
--
ALTER TABLE `tbl_invitedusers`
  ADD CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`);

--
-- Constraints for table `tbl_lists`
--
ALTER TABLE `tbl_lists`
  ADD CONSTRAINT `fk_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_list_contacts`
--
ALTER TABLE `tbl_list_contacts`
  ADD CONSTRAINT `tbl_list_contacts_fk_list_id` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_list_contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`);

--
-- Constraints for table `tbl_newsletter`
--
ALTER TABLE `tbl_newsletter`
  ADD CONSTRAINT `tbl_newsletter_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `fk_profiels_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_template`
--
ALTER TABLE `tbl_template`
  ADD CONSTRAINT `fk_template` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;
