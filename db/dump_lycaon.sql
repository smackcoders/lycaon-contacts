-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: smack_lycaon
-- ------------------------------------------------------
-- Server version	5.1.63-0ubuntu0.11.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mail_conf`
--

DROP TABLE IF EXISTS `mail_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_conf` (
  `host_name` varchar(120) DEFAULT NULL,
  `port_no` varchar(20) DEFAULT NULL,
  `username` varchar(180) DEFAULT NULL,
  `password` varchar(180) DEFAULT NULL,
  `encrypt_key` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_conf`
--

LOCK TABLES `mail_conf` WRITE;
/*!40000 ALTER TABLE `mail_conf` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_conf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_drafts`
--

DROP TABLE IF EXISTS `tbl_drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_drafts` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `saved_by` int(11) NOT NULL,
  `from_name` varchar(125) NOT NULL,
  `from_email` varchar(125) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `message` longtext,
  `template_id` int(11) DEFAULT NULL,
  `saved_at` varchar(60) DEFAULT NULL,
  `lists` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_drafts`
--

LOCK TABLES `tbl_drafts` WRITE;
/*!40000 ALTER TABLE `tbl_drafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_drafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invitedusers`
--

DROP TABLE IF EXISTS `tbl_invitedusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invitedusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviter_id` int(11) NOT NULL,
  `email_service` varchar(20) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `subscription` tinyint(1) DEFAULT '1',
  `subscription_key` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_contact_id` (`contact_id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invitedusers`
--

LOCK TABLES `tbl_invitedusers` WRITE;
/*!40000 ALTER TABLE `tbl_invitedusers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_invitedusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invites`
--

DROP TABLE IF EXISTS `tbl_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invites` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `work_phone` varchar(20) DEFAULT NULL,
  `home` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `company` varchar(125) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invites`
--

LOCK TABLES `tbl_invites` WRITE;
/*!40000 ALTER TABLE `tbl_invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_list_contacts`
--

DROP TABLE IF EXISTS `tbl_list_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_list_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `subscription` tinyint(1) DEFAULT '1',
  `subscription_key` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `tbl_list_contacts_fk_list_id` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_list_contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_list_contacts`
--

LOCK TABLES `tbl_list_contacts` WRITE;
/*!40000 ALTER TABLE `tbl_list_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_list_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_listadmins`
--

DROP TABLE IF EXISTS `tbl_listadmins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_listadmins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `permission` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `tbl_listadmins_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_listadmins`
--

LOCK TABLES `tbl_listadmins` WRITE;
/*!40000 ALTER TABLE `tbl_listadmins` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_listadmins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lists`
--

DROP TABLE IF EXISTS `tbl_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lists` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_time` int(10) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `fk_owner_id` (`owner_id`),
  CONSTRAINT `fk_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lists`
--

LOCK TABLES `tbl_lists` WRITE;
/*!40000 ALTER TABLE `tbl_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `from_name` varchar(125) NOT NULL,
  `from_email` varchar(125) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `message` longtext,
  `template_id` int(11) DEFAULT NULL,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `template_id` (`template_id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `tbl_newsletter_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter`
--

LOCK TABLES `tbl_newsletter` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles`
--

DROP TABLE IF EXISTS `tbl_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) DEFAULT '',
  `firstname` varchar(50) DEFAULT '',
  `birthday` date DEFAULT '0000-00-00',
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `fk_profiels_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles`
--

LOCK TABLES `tbl_profiles` WRITE;
/*!40000 ALTER TABLE `tbl_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles_fields`
--

DROP TABLE IF EXISTS `tbl_profiles_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles_fields`
--

LOCK TABLES `tbl_profiles_fields` WRITE;
/*!40000 ALTER TABLE `tbl_profiles_fields` DISABLE KEYS */;
INSERT INTO `tbl_profiles_fields` VALUES (1,'lastname','Last Name','VARCHAR',50,3,1,'','','Incorrect Last Name (length between 3 and 50 characters).','','','','',1,3),(2,'firstname','First Name','VARCHAR',50,3,1,'','','Incorrect First Name (length between 3 and 50 characters).','','','','',0,3),(3,'birthday','Birthday','DATE',0,0,2,'','','','','0000-00-00','UWjuidate','{\"ui-theme\":\"redmond\"}',3,2);
/*!40000 ALTER TABLE `tbl_profiles_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reports`
--

DROP TABLE IF EXISTS `tbl_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_name` varchar(125) NOT NULL,
  `sender_mail` varchar(125) NOT NULL,
  `newsletter` longtext,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `receiver_mail` varchar(125) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `status_key` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reports`
--

LOCK TABLES `tbl_reports` WRITE;
/*!40000 ALTER TABLE `tbl_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `fk_template` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_template`
--

LOCK TABLES `tbl_template` WRITE;
/*!40000 ALTER TABLE `tbl_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `org_name` varchar(75) DEFAULT NULL,
  `org_desc` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `adminuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `content` varchar(55540) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES ('&lt;html&gt;\n&lt;head&gt;\n&lt;style type=&quot;text/css&quot;&gt;\np{\ncolor:Black;\nfont-size:20px;\n}\nspan{\ncolor:red;\n}\nbody{\n border:1px solid #6699ff;\nwidth:250px;\nheight:150px;	\n}\n\n&lt;/style&gt;\n&lt;/head&gt;\n&lt;body&gt;\n&lt;a href=\'www.google.com\'&gt;&lt;img src=\'image.jpeg\' , alt =\'SmackCoders\'&gt;&lt;/a&gt;&lt;br /&gt;\n&lt;p&gt;Thanks for registering at &lt;span&gt;Smackers&lt;/span&gt; &lt;br /&gt;You need to verify your email address before you can start using our service.&lt;/p&gt;&lt;br /&gt;\n&lt;p&gt;Please click on the link below in order to verify your email address and activate your account at Smacker Protocal&lt;/p&gt;&lt;br /&gt;\n\n&lt;/body&gt;\n&lt;/html&gt;\n','text');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-03 20:11:50
