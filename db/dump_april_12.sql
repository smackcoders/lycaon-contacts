-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: newsletter
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_invitedusers`
--

DROP TABLE IF EXISTS `tbl_invitedusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invitedusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviter_id` int(11) NOT NULL,
  `email_service` varchar(20) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `subscription` tinyint(1) DEFAULT '1',
  `subscription_key` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_contact_id` (`contact_id`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invitedusers`
--

LOCK TABLES `tbl_invitedusers` WRITE;
/*!40000 ALTER TABLE `tbl_invitedusers` DISABLE KEYS */;
INSERT INTO `tbl_invitedusers` VALUES (3,14,'gmail',3,0,1,'fd4c596ffafff8efcc73e6eae37263dc'),(4,14,'gmail',4,0,1,'43cde13b27e7565cb90d0c6e9e2afcc4'),(6,14,'gmail',6,0,1,'b8179d0e5723b48055dd1236be83e90f'),(7,14,'gmail',7,0,1,'2fc1421e4d8fecd1291916bc34318803'),(12,18,'gmail',4,0,1,'165c4e015059dfa0994582ff3268a76d'),(13,18,'gmail',9,0,1,'496363c2fdcd428469f4e4542f1bc107'),(14,18,'gmail',1,0,1,'1aec8f2b8f5b49aad84333294322f0c6'),(15,18,'gmail',3,0,1,'fc63ec2fadb4df37e8943f2fd176acc7'),(16,18,'gmail',6,0,1,'674a10ea483eb2342316815ff3d5062f'),(17,18,'gmail',7,0,1,'f77fd27b492f2ec2cb4756b6eb878004'),(18,18,'gmail',11,0,1,'257e1849d8f446e44b77f1bf0623cdbe'),(19,18,'gmail',2,0,1,'554180ec03ca76929e2ac18bdd952296'),(20,18,'gmail',8,0,1,'2b0404b18738f3ca9714536c2fdc74b9');
/*!40000 ALTER TABLE `tbl_invitedusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invites`
--

DROP TABLE IF EXISTS `tbl_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invites` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `work_phone` varchar(20) DEFAULT NULL,
  `home` varchar(20) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `anniversary` varchar(20) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invites`
--

LOCK TABLES `tbl_invites` WRITE;
/*!40000 ALTER TABLE `tbl_invites` DISABLE KEYS */;
INSERT INTO `tbl_invites` VALUES (1,NULL,NULL,NULL,'Joseph Asir Raja',NULL,NULL,NULL,NULL,NULL,'josephs@smackcoders.com',1,0),(2,NULL,NULL,NULL,'Prem Kumar',NULL,NULL,NULL,NULL,NULL,'premkumarr@smackcoders.com',1,0),(3,NULL,NULL,NULL,'Fredrick Sujindoss',NULL,NULL,NULL,NULL,NULL,'fredrickm@smackcoders.com',1,0),(4,NULL,NULL,NULL,'Rajue Murugiah',NULL,NULL,NULL,NULL,NULL,'rajue@smackcoders.com',1,0),(5,NULL,NULL,NULL,'Fredrick',NULL,NULL,NULL,NULL,NULL,'fredrick.sujin@gmail.com',1,0),(6,NULL,NULL,NULL,'Arul Manickam',NULL,NULL,NULL,NULL,NULL,'arulmanickams@smackcoders.com',1,0),(7,NULL,NULL,NULL,'Fenzik Joseph',NULL,NULL,NULL,NULL,NULL,'fenzik@smackcoders.com',1,0),(8,NULL,NULL,NULL,'Muthu Kumar',NULL,NULL,NULL,NULL,NULL,'muthukumarp@smackcoders.com',1,0),(9,NULL,NULL,NULL,'Rajkumar M',NULL,NULL,NULL,NULL,NULL,'rajkumarm@smackcoders.com',1,0),(10,'','','','','','','','','','mailmefredrick@gmail.com',1,0),(11,NULL,NULL,NULL,'S.A.A.RAJESWAR MHURGAIYA',NULL,NULL,NULL,NULL,NULL,'mhurgaiya@gmail.com',1,0),(12,'','','','arul','','','','','','arulmanickams@gmail.com',1,0);
/*!40000 ALTER TABLE `tbl_invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_list_contacts`
--

DROP TABLE IF EXISTS `tbl_list_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_list_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `tbl_list_contacts_fk_list_id` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_list_contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `tbl_invites` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_list_contacts`
--

LOCK TABLES `tbl_list_contacts` WRITE;
/*!40000 ALTER TABLE `tbl_list_contacts` DISABLE KEYS */;
INSERT INTO `tbl_list_contacts` VALUES (3,1,3,14),(4,1,6,14),(6,2,3,14),(7,2,4,14);
/*!40000 ALTER TABLE `tbl_list_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lists`
--

DROP TABLE IF EXISTS `tbl_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lists` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_time` int(10) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `fk_owner_id` (`owner_id`),
  CONSTRAINT `fk_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lists`
--

LOCK TABLES `tbl_lists` WRITE;
/*!40000 ALTER TABLE `tbl_lists` DISABLE KEYS */;
INSERT INTO `tbl_lists` VALUES (1,'group','description',1334182404,14),(2,'smackers','officiallist',1334182443,14);
/*!40000 ALTER TABLE `tbl_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `from_name` varchar(125) NOT NULL,
  `from_email` varchar(125) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `message` longtext,
  `template_id` int(11) DEFAULT NULL,
  `scheduled_at` varchar(60) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `template_id` (`template_id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `tbl_newsletter_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `tbl_lists` (`list_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter`
--

LOCK TABLES `tbl_newsletter` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles`
--

DROP TABLE IF EXISTS `tbl_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) DEFAULT '',
  `firstname` varchar(50) DEFAULT '',
  `birthday` date DEFAULT '0000-00-00',
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `fk_profiels_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles`
--

LOCK TABLES `tbl_profiles` WRITE;
/*!40000 ALTER TABLE `tbl_profiles` DISABLE KEYS */;
INSERT INTO `tbl_profiles` VALUES (13,'jasper','rajkumar','2012-04-04'),(14,'sujindoss','fredrick','1989-05-01'),(18,'saa','rajue','2003-06-13'),(19,'','muthu','0000-00-00');
/*!40000 ALTER TABLE `tbl_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profiles_fields`
--

DROP TABLE IF EXISTS `tbl_profiles_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profiles_fields`
--

LOCK TABLES `tbl_profiles_fields` WRITE;
/*!40000 ALTER TABLE `tbl_profiles_fields` DISABLE KEYS */;
INSERT INTO `tbl_profiles_fields` VALUES (1,'lastname','Last Name','VARCHAR',50,3,1,'','','Incorrect Last Name (length between 3 and 50 characters).','','','','',1,3),(2,'firstname','First Name','VARCHAR',50,3,1,'','','Incorrect First Name (length between 3 and 50 characters).','','','','',0,3),(3,'birthday','Birthday','DATE',0,0,2,'','','','','0000-00-00','UWjuidate','{\"ui-theme\":\"redmond\"}',3,2);
/*!40000 ALTER TABLE `tbl_profiles_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `fk_template` FOREIGN KEY (`owner_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_template`
--

LOCK TABLES `tbl_template` WRITE;
/*!40000 ALTER TABLE `tbl_template` DISABLE KEYS */;
INSERT INTO `tbl_template` VALUES (3,'Test','\r\n&lt;title&gt;Template&lt;/title&gt;\r\n\r\n[CONTENT]\r\n	&lt;div style=&quot;background-color:#CACAC4;width:250px;height:150px;&quot;&gt;\r\n		&lt;h2&gt;Welcome,&lt;/h2&gt;\r\n		&lt;div style=&quot;margin-left:35px;margin-top:35px;&quot;&gt;\r\n			You Got a new mail!.\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n\r\n                 \r\n\r\n\r\n',14);
/*!40000 ALTER TABLE `tbl_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `org_name` varchar(75) DEFAULT NULL,
  `org_desc` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','webmaster@example.com','','','','','','0',NULL,'9a24eff8c15a6a141ece27eb6947da0f',1261146094,0,1,1),(2,'demo','fe01ce2a7fbac8fafaed7c982a04e229','demo@example.com','','','','','','0',NULL,'099f825543f7850cc038b90aaff39fac',1261146096,0,0,1),(8,'arul','be3025c4341e340ee641e89779ca2649','arulmanickams@smackcoders.com','smackcoders','it','tvl','tamil nadu','india','9003777515','manickamda','9907b2497db4da1ae2d261a856fd001d',1332597314,1333554003,0,1),(9,'rajue','031700427b70a87b0203a7d78a85c0da','rajue@smackcoders.com','smack','smack','chennai','tn','india','9865354604','elinkbox.in','e77d01a9b4f2fbb294b97da0f26a2172',1332845784,1332845922,0,1),(10,'sample','5e8ff9bf55ba3508199d22e984129be6','premkumarr@smackcoders.com','smackcoders','IT Company','Trunelveli','Tamilnadu','India','9999999999','','571f5323340a0e8a5c9d8dab05c4a322',1332941001,0,0,0),(13,'rajkumar','79cfac6387e0d582f83a29a04d0bcdc4','rajkumarm@smackcoders.com','smackcoder','IT','Thirunelveli','TamilNadu','India','9629211536','qwerty','7e63a0d982a45f6691eaaf6aeee32002',1334081949,1334082258,0,1),(14,'sujin','55d674f40a42cb80806e91ad42699311','fredrickm@smackcoders.com','smackcoders','IT','Tirunelveli','Tamilnadu','India','9629211536','www.fredrick.blogspot.com','1866abed80b590541a0b28393abf77ed',1334082988,1334183808,0,1),(18,'rajue1','9cf9ee00ea6d6e448e6b176fe1ba715d','mhurgaiya@gmail.com','pixi','PIXI','chennai','tn','india','9865354604','','95dc63d513efb9c1e13be5ed733b627e',1334159367,1334159653,0,1),(19,'muthu','79cfac6387e0d582f83a29a04d0bcdc4','muthukumarp@smackcoders.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'433489a11c6d54f7e1160b7b6bebb2cd',1334159571,1334159633,0,1);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger insert_in_tbl_users_profiles after insert on
tbl_users for each row
begin
insert into tbl_profiles (user_id,firstname)values(new.id,new.username);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger eliniminate_tbl_users_profiles after delete on
tbl_users for each row 
begin
delete from tbl_profiles where user_id = old.id;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `content` varchar(55540) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES ('&lt;html&gt;\n&lt;head&gt;\n&lt;style type=&quot;text/css&quot;&gt;\np{\ncolor:Black;\nfont-size:20px;\n}\nspan{\ncolor:red;\n}\nbody{\n border:1px solid #6699ff;\nwidth:250px;\nheight:150px;	\n}\n\n&lt;/style&gt;\n&lt;/head&gt;\n&lt;body&gt;\n&lt;a href=\'www.google.com\'&gt;&lt;img src=\'image.jpeg\' , alt =\'SmackCoders\'&gt;&lt;/a&gt;&lt;br /&gt;\n&lt;p&gt;Thanks for registering at &lt;span&gt;Smackers&lt;/span&gt; &lt;br /&gt;You need to verify your email address before you can start using our service.&lt;/p&gt;&lt;br /&gt;\n&lt;p&gt;Please click on the link below in order to verify your email address and activate your account at Smacker Protocal&lt;/p&gt;&lt;br /&gt;\n\n&lt;/body&gt;\n&lt;/html&gt;\n','text');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-12  4:26:27
