﻿<!DOCTYPE HTML>
<html >

<head>

<title>Sociojly - Social CRM and cPanel</title>
<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/form.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/super.css" />
-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/smack.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/childf1.css" />

<link rel="shortcut icon" href="images/favicon.ico">
<link rel=stylesheet href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/css/regform.css" type="text/css" media=all>

<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/images/favicon.ico"> 


</head>

<body>

<div id="top-content">
	<div class="logo"></div>


	<span class="tools tools-all"><a href="#"> <img style="border: medium none;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/images/icon32/bookmarking.png"> </a> </span>
	<span class="tools tools-all"><a href="#"> <img style="border: medium none;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/images/icon32/youtube.png"> </a> </span>
	<span class="tools tools-all"><a href="#"> <img style="border: medium none;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/images/icon32/facebook.png"> </a> </span>
	<span class="tools tools-all"><a href="#"> <img style="border: medium none;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/normal/views/layouts/images/icon32/twitter.png"> </a> </span>


	<div class="member-box"> 
		<?php $this->widget('application.extensions.mbmenu.MbMenu',array('items'=>array(
					array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)),	
					)); ?> 
				</div>

		<span class="wallet"><a href="#">Wallet 256,358,900</a> </span>
		<span class="member-box"> 

		<!--if not logged in display this--> 
		<!-- <a href="#">Login </a>  <a href="#">Sign in with twitter</a>-->

		<!--or display member status info section--> 

		<a style="color: red;" href="#">Username</a>	<a style="color: white;" href="#"> Rank - # 2785656</a> 

		</span>

	</div>

</div>

<div id="main-content">


	<div id="wide-content">
		<div id="page-nav">




<div class=tab-group>

<?php echo CHtml::link('<div class="channel switcher" onclick="return switchChan("channel");"><div class=name>Account</div></div>',array('/user/profile/profile')); ?>
<?php //echo CHtml::link('Account',array('/user/profile/account')); ?>

<div class="channel switcher  activechan" onclick="return switchChan('channel');">
<div class=name>Twitter</div>
</div>
<div class="channel switcher" onclick="return switchChan('channel');">
<div class=name>Facebook</div>
</div>
<div class="channel switcher" onclick="return switchChan('channel');">
<div class=name>LinkedIn</div>
</div>
<div class="channel switcher" onclick="return switchChan('channel');">
<div class=name>Youtube</div>
</div>
<div class="channel switcher" onclick="return switchChan('channel');">
<div class=name>Web/Blog</div>
</div>
</div>

<!--
<div id="main_nav">
					<?php $this->widget('zii.widgets.CMenu',array('items'=>array(
						array('label'=>'Account', 'url'=>array('/user/profile/profile')),
						array('label'=>'Twitter', 'url'=>array('/user/profile/twitter')),
						array('label'=>'Profile', 'url'=>array('/user/accounts/social')),
						array('label'=>'Facebook', 'url'=>array('/user/profile/facebook')),
						array('label'=>'LinkedIn', 'url'=>array('/user/profile/linkedin')),	
						array('label'=>'Other', 'url'=>array('/site/pro')),
						array('label'=>'Srbac', 'url'=>array('/srbac'), 'visible'=>false)),					
					)); ?>
</div>
-->
		</div>
		<div id="page-content">
		
		<?php echo $content; ?>	


		</div>
	</div>

</div>




</body>
</html>
