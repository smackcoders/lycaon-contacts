<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invite-import-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inviter_id'); ?>
		<?php echo $form->textField($model,'inviter_id'); ?>
		<?php echo $form->error($model,'inviter_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'blacklist'); ?>
		<?php echo $form->textField($model,'blacklist'); ?>
		<?php echo $form->error($model,'blacklist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact_name'); ?>
		<?php echo $form->textField($model,'contact_name'); ?>
		<?php echo $form->error($model,'contact_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_service'); ?>
		<?php echo $form->textField($model,'email_service'); ?>
		<?php echo $form->error($model,'email_service'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->