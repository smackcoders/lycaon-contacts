<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>
<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>

<!--<p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>-->

<div class="form">
<?php echo CHtml::beginForm(); ?>

	
	<?php echo CHtml::errorSummary($model); ?>
<div id="login_pag" class="pagearea">
<!--	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>-->

<div class="row">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username') ?>
</div>

<div class="row">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password') ?>
</div>
<div class="row">
		<?php echo CHtml::activeCheckBox($model,'rememberMe',array('class'=>'remp_check')); ?>
		<?php echo CHtml::activeLabelEx($model,'rememberMe',array('class'=>'remp')); ?>
		<?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'button')); ?>
</div>
<div class="row"><span class="seperator"></span> <?php echo CHtml::link(UserModule::t("Forget Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
<span class="seperator_ver"></span>
		<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> 

 </div>
</div>

	
<?php echo CHtml::endForm(); ?>
</div><!-- form -->


<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
            'class'=>'button',
        ),
    ),
), $model);
?>
