<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Contacts'),array('/user/invite/index'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import & Export'),array('/user/invite'),array( 'class'=>'uiButton')); ?> 
</div>
<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Add Contact");
$this->breadcrumbs=array(
	UserModule::t("Invite")=>array('index'),
	UserModule::t("Add Contact"),
);
?>
<?php
$this->menu=array(
	array('label'=>'Contacts', 'url'=>array('/user/invite/index')),
	array('label'=>'List Contacts', 'url'=>array('/user/list')),
	array('label'=>'Import Contacts', 'url'=>array('/user/invite')),
	array('label'=>'Manage Contacts', 'url'=>array('/user/invite/admin')),
);
?>
<?php if(Yii::app()->user->hasFlash('addnew')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('addnew'); ?>
</div>
<?php else: 
endif; ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invite-addnew-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

<div id="inner">
	<div class="leftcontent">
	<div id="content" class="pagearea">
	<h3>New Contact</h3>	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<form id="new_contact" class="new_contact" method="post" action="">
	<div class="ui-form formm">
	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name'); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name'); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'Date of Birth'); ?>
		<!--<?php echo $form->textField($model,'dob'); ?>-->
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'name'=>'dob',
			'language'=>Yii::app()->language=='et' ? 'et' : null,
			'options'=>array(
			'showAnim'=>'fold', // 'show' (the default), 'slideDown', 'fadeIn', 'fold'
			'showOn'=>'button', // 'focus', 'button', 'both'
			'buttonText'=>Yii::t('ui','Select form calendar'), 
			'buttonImage'=>Yii::app()->request->baseUrl.'/images/calendar.png', 
			'buttonImageOnly'=>true,
    		),
	    	'htmlOptions'=>array(
	            'style'=>'width:225px;vertical-align:top'
		    ),  
		));
		?>
		<?php echo $form->error($model,'dob'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'company'); ?>
		<?php echo $form->textField($model,'company'); ?>
		<?php echo $form->error($model,'company'); ?>
	</div>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'contact_name'); ?>
		<?php echo $form->textField($model,'contact_name'); ?>
		<?php echo $form->error($model,'contact_name'); ?>
	</div>-->
	<div class="row">
		<?php echo $form->labelEx($model,'work_phone'); ?>
		<?php echo $form->textField($model,'work_phone'); ?>
		<?php echo $form->error($model,'work_phone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'home'); ?>
		<?php echo $form->textField($model,'home'); ?>
		<?php echo $form->error($model,'home'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'mobile_phone'); ?>
		<?php echo $form->textField($model,'mobile_phone'); ?>
		<?php echo $form->error($model,'mobile_phone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax'); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>



	</div>
	<div class="button-container">
		<?php echo CHtml::submitButton('Save',array('class'=>'uiButton', 'type'=>'submit' )); ?>
		<?php echo CHtml::submitButton('Save and Continue',array('class'=>'uiButton')); ?>
	</div>
	</form>
	</div>
	</div>
	</div>


<!--	<div class="row">
		<?php echo $form->labelEx($model,'blacklist'); ?>
		<?php echo $form->textField($model,'blacklist'); ?>
		<?php echo $form->error($model,'blacklist'); ?>
	</div>-->


<?php $this->endWidget(); ?>

</div><!-- form -->

