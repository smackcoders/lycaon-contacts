<?php
$this->breadcrumbs=array(
	'Contact'=>array('index'),
	$model->contact_name,
);

$this->menu=array(
	array('label'=>'List Invite', 'url'=>array('index')),
	array('label'=>'Create Invite', 'url'=>array('create')),
	array('label'=>'Update Invite', 'url'=>array('update', 'id'=>$model->contact_id)),
	array('label'=>'Delete Invite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->contact_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invite', 'url'=>array('admin')),
);
?>

<img alt='user' src=' <?php echo Yii::app()->request->baseUrl."/css/contacts_imgs/profile_blank_thumb.gif"; ?> ' />
<div class="item_information">
	<b class="item_info"><?php echo $model->contact_name; ?></b>
	<div class="info_data">
		<span><?php echo $model->email; ?></span>
	</div>
</div>
<!--<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'contact_id',
		'first_name',
		'last_name',
		'dob',
		'contact_name',
		'work_phone',
		'home',
		'mobile_phone',
		'fax',
		'company',
		//'email',
		//'status',
		//'blacklist',
	),
)); ?>-->
<div class="pagearea prof_pag">
<table class="dataGrid content_list ui-form prof_page" >
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('First Name')); ?>
</th>
    <td> <?php echo $model->first_name; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Last Name')); ?>
</th>
    <td><?php echo $model->last_name; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Date of Birth')); ?>
</th>
    <td> <?php echo $model->dob; ?>
</td>
</tr>

<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Contact Name')); ?>
</th>
    <td> <?php echo $model->contact_name; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Work Phone')); ?>
</th>
    <td><?php echo $model->work_phone; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Home Phone')); ?>
</th>
    <td><?php echo $model->home; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Mobile Phone')); ?>
</th>
    <td> <?php echo $model->mobile_phone; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Fax')); ?>
</th>
    <td><?php echo $model->fax; ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('Company')); ?>
</th>
    <td><?php echo $model->company; ?>
</td>
</tr>
</table>
</div>
