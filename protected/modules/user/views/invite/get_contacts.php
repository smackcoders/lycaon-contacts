<?php
$this->breadcrumbs=array(
	'Import Contact',
);?>
<div style="float:right;margin-top:-40px;">
	<?php echo CHtml::link(UserModule::t('Contacts'),array('/user/invite/index'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Add Contacts'),array('/user/invite'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import & Export'),array('/user/invite'),array( 'class'=>'uiButton')); ?>
</div>

<?php
$contents="<html>
<head>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js' type='text/javascript' charset='utf-8'></script>
<script src='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/jquery.uniform.min.js' type='text/javascript' charset='utf-8'></script>
<script type='text/javascript' charset='utf-8'>
  $(function(){
	$('input, textarea, select, button').uniform();
  });
</script>
<link rel='stylesheet' href='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/uniform.default.css' type='text/css' media='screen'>
<style type='text/css' media='screen'>
body {
font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
color: #666;
}
h1 {
margin-top: 0;
}
ul {
list-style: none;
padding: 0;
margin: 0;
}
li {
margin-bottom: 20px;
clear: both;
}
label {
font-size: 10px;
font-weight: bold;
text-transform: uppercase;
display: block;
margin-bottom: 3px;
clear: both;
}
</style>
</head>
";
include('openinviter/openinviter.php');
$inviter=new OpenInviter();
global $oi_services;
$oi_services=$inviter->getPlugins();
$pluginContent="";
global $plugType;
if (isset($_POST['provider_box'])) 
	{
	if (isset($oi_services['email'][$_POST['provider_box']])) $plugType='email';
	elseif (isset($oi_services['social'][$_POST['provider_box']])) $plugType='social';
	else $plugType='';
	if ($plugType) $pluginContent=createPluginContent($_POST['provider_box']);
	}
elseif(!empty($_GET['provider_box']))
	{
	if (isset($oi_services['email'][$_GET['provider_box']])) $plugType='email';
	elseif (isset($oi_services['social'][$_GET['provider_box']])) $plugType='social';
	else $plugType='';
	if($plugType) 
		{ 
		$_POST['provider_box']=$_GET['provider_box'];
		$pluginContent=createPluginContent($_GET['provider_box']);
		}
	}
else { $plugType = ''; $_POST['provider_box']=''; }

function ers($ers)
	{

	if (!empty($ers))
		{
		$contents="<table cellspacing='0' cellpadding='0' style='border:1px solid red;' align='center'><tr><td valign='middle' style='padding:3px' valign='middle'><img src='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/imgs/ers.gif'></td><td valign='middle' style='color:red;padding:5px;'>";
		foreach ($ers as $key=>$error)
			$contents.="{$error}<br >";
		$contents.="</td></tr></table><br >";
		return $contents;
		}
	}
function oks($oks)
	{
	if (!empty($oks))
		{
		$contents="<table border='0' cellspacing='0' cellpadding='10' style='border:1px solid #5897FE;' align='center'><tr><td valign='middle' valign='middle'><img src='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/imgs/oks.gif' ></td><td valign='middle' style='color:#5897FE;padding:5px;'>	";
		foreach ($oks as $key=>$msg)
			$contents.="{$msg}<br >";
		$contents.="</td></tr></table><br >";
		return $contents;
		}
	}

if (!empty($_POST['step']))$step=$_POST['step'];

else $step='get_contacts';

$ers=array();$oks=array();$import_ok=false;$done=false;
if ($_SERVER['REQUEST_METHOD']=='POST')
	{ 
	if ($step=='get_contacts')
		{
		if (empty($_POST['email_box']))
			$ers['email']="Email missing !";
		if (empty($_POST['password_box']))
			$ers['password']="Password missing !";	
		if (empty($_POST['provider_box']))
			$ers['provide']='Provider missing!';
		if (count($ers)==0)
			{
			$inviter->startPlugin($_POST['provider_box']);
			$internal=$inviter->getInternalError();//print('<pre>');print_r($oi_services['email']);
			if ($internal)
				$ers['inviter']=$internal;
			elseif (!$inviter->login($_POST['email_box'],$_POST['password_box']))
				{
				$internal=$inviter->getInternalError();
				$ers['login']=($internal?$internal:"Login failed. Please check the email and password you have provided and try again later !");
				}
			elseif (false===$contacts=$inviter->getMyContacts())
				$ers['contacts']="Unable to get contacts !";
			else
				{
				$import_ok=true;
				$step='import';
				$_POST['oi_session_id']=$inviter->plugin->getSessionID();
				$_POST['message_box']='';
				}
			}
		}
	elseif ($step=='import')
		{
		if (empty($_POST['provider_box'])) $ers['provider']='Provider missing !';
		else
			{
			$inviter->startPlugin($_POST['provider_box']);
			$internal=$inviter->getInternalError();
			if ($internal) $ers['internal']=$internal;
			else
				{
				if (empty($_POST['email_box'])) $ers['inviter']='Inviter information missing !';
				if (empty($_POST['oi_session_id'])) $ers['session_id']='No active session !';
				//if (empty($_POST['message_box'])) $ers['message_body']='Message missing !';
				else $_POST['message_box']=strip_tags($_POST['message_box']);
				$selected_contacts=array();$contacts=array();

				$message=array('subject'=>$inviter->settings['message_subject'],'body'=>$inviter->settings['message_body'],'attachment'=>"\n\rAttached message: \n\r".$_POST['message_box']);
				if ($inviter->showContacts())
					{
					foreach ($_POST as $key=>$val)
						if (strpos($key,'check_')!==false)
							$selected_contacts[$_POST['email_'.$val]]=$_POST['name_'.$val];
						elseif (strpos($key,'email_')!==false)
							{
							$temp=explode('_',$key);$counter=$temp[1];
							if (is_numeric($temp[1])) $contacts[$val]=$_POST['name_'.$temp[1]];
							}
					if (count($selected_contacts)==0) $ers['contacts']="You haven't selected any contacts to invite !";
					}
				}
			}

		}
	}
else
	{
	$_POST['email_box']='';
	$_POST['password_box']='';
	}
$contents.='<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
function toggleChecked(status) {
$(".checkbox").each( function() {
$(this).attr("checked",status);
})
}
</script>';

$contents.="<form action='' method='POST' name='openinviter'>".ers($ers).oks($oks);
$contents.="<div style='background-color:#F5F5F5;'>"; //want to change background of popup div..
$contents.="<center>{$pluginContent}</center>";

if (!$done)
	{
	if ($step=='get_contacts')
		{
		$contents.="<table align='center' class='thTable' cellspacing='2' cellpadding='0' style='border:none;'>
			<tr><td align='right'><label for='email_box'>Email</label></td><td><input type='text' name='email_box' value='{$_POST['email_box']}'></td></tr>
			<tr><td align='right'><label for='password_box'>Password</label></td><td><input type='password' name='password_box' value='{$_POST['password_box']}'></td></tr>
			<tr><td colspan='2' align='center'><input type='submit' name='import' value='Import Contacts'></td></tr>
			<tr><td colspan='2' align='center'><br/><br/><h6>We will not store you password. by <a href='http://www.smackerportal.com'  target='_blank'>smackerportal.com<h6></td></tr>
		</table><input type='hidden' name='provider_box' value='{$_POST['provider_box']}.plg'><input type='hidden' name='step' value='get_contacts'>";

		}
	}

if (!$done)
	{
	if ($step=='import')
		{

		if ($inviter->showContacts())
			{
			$contents.="<div align='left'><span><b>Contacts:</b></span><span align='center' style='width:40px;'>".count($contacts)."</span><span style='width:200px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Contacts Selected:</b></span><span align='center' style='width:40px;' id='divselected'></span></div><br/>";
			if (count($contacts)==0)
				$contents.="<div align='left' style='padding:20px;' colspan='".($plugType=='email'? "3":"2")."'>You do not have any contacts in your address book.</div>";
			else
				{
				$contents.="<div style='width:100px;float:left;'><input type='checkbox' onClick='toggleChecked(this.checked)' name='toggle_all' class='check' title='    Select/Deselect all' checked></div>Select/Deselect all</td><td style='padding-left:10px;'>";
				$counter=0;
				foreach ($contacts as $email=>$name)
					{
					$counter++;
					$contents.="<input type='hidden' name='contacts[{$email}]' value='{$name}'>";			
					$contents.="<div style='margin-left:3px;margin-top:5px;background-color:#F0F0F0 ;border-top-left-radius:2px;border-top-right-radius:2px;border-bottom-left-radius:2px;border-bottom-right-radius:2px;'>";
					//$contents.='<div class="disp">';
					$contents.="<div style='width:100px;float:left;'><input name='check_{$counter}' value='{$counter}' class='checkbox' type='checkbox' checked><input type='hidden' name='email_{$counter}' value='{$email}'></div><input type='hidden' name='name_{$counter}' value='{$name}' ><h5>{$name}</h5>".($plugType == 'email' ?"{$email}":"")."</div>";
					}
				$contents.="<div><tr><td colspan='".($plugType=='email'? "3":"2")."' style='padding:3px;'><input type='submit' name='send' value='Import Contacts'></td></tr></div>";
				$contents.="<div><h6>Please invite friends only whom knows you.<a href='http://localhost/sociojly/index.php/user/profile/account' target='_blank'>learn more</a></h6></div>";				
				}
			
			$contents.="</div>";
			}

		$contents.="<input type='hidden' name='step' value='import'><br/>
			<input type='hidden' name='provider_box' value='{$_POST['provider_box']}'>
			<input type='hidden' name='email_box' value='{$_POST['email_box']}'>
			<input type='hidden' name='oi_session_id' value='{$_POST['oi_session_id']}'>";
		$contents.="<script>self.parent.$.fancybox.resize();</script>";
		
		}
	}
$contents.="<div>";
$contents.="</form>";
$contents.="<script>
function countChecked() {
if($('.check:checked'))
{
  var n = $('.checkbox:checked').length;
  $('#divselected').text(n);
}
else
{
  $('#divselected').text(0);
}
}
countChecked();
$('.checkbox').click(countChecked);
$('.check').click(countChecked);
</script>
";

echo $contents;

function createPluginContent($pr)
	{
	if ($pr=='gmail')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/gmail.png\");'></div>";
	else if ($pr=='hotmail')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/hotmail.png\");'></div>";
	else if ($pr=='yahoo')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/yahoo.png\");'></div>";
	else if ($pr=='facebook')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/facebook.png\");'></div>";
	else if ($pr=='twitter')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/twitter.png\");'></div>";
	else if ($pr=='linkedin')
	return $contentPlugin="<div style='border:none; display:block; height:60px; margin:0; padding:0; width:130px; background-image:url(\"".Yii::app()->request->baseUrl."/css/contacts_imgs/linkedin.png\");'></div>";
	}
?>
</div>
</div>
