<?php
if(isset($_GET['id'])){
	$command=Yii::app()->db->createCommand();
	$inviter=Yii::app()->user->id;
	$command->delete('tbl_invitedusers','inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$_GET['id']));
	$datareader=$command->execute();
	$command=Yii::app()->db->createCommand();
	$command->select('list_id,contact_id');
	$command->from('tbl_list_contacts');
	$command->where("owner_id=:owner",array(":owner"=>$inviter));
	$datareader=$command->query();
	$dr=$datareader->readAll();
	foreach($dr as $key=>$value){
		$arr[]=$value['list_id'];
		if(isset($value['contact_id'])){
			if($value['contact_id']==$_GET['id']){
				$command=Yii::app()->db->createCommand();
				$command->delete('tbl_list_contacts','contact_id=:contact', array(":contact"=>$_GET['id']));
				$datareader=$command->execute();	
			}
		}
	}
	$this->redirect(array('index'));	
}
?>
