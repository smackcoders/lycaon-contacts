<?php
$this->breadcrumbs=array(
	'Invites'=>array('index'),
	$model->contact_name=>array('view','id'=>$model->contact_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Invite', 'url'=>array('index')),
	array('label'=>'Create Invite', 'url'=>array('create')),
	array('label'=>'View Invite', 'url'=>array('view', 'id'=>$model->contact_id)),
	array('label'=>'Manage Invite', 'url'=>array('admin')),
);
?>

<h1>Update <?php echo $model->contact_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
