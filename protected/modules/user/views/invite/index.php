<?php
$this->breadcrumbs=array(
	'Contacts',
);
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Add Contact'),array('/user/invite/addnew'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import Contacts'),array('/user/invite'),array( 'class'=>'uiButton')); ?>  <a href="<?php echo Yii::app()->baseUrl; ?>/index.php?r=user/invite/invites_export" class="uiButton" >Export Contacts</a>  <a id="displayText" href="javascript:toggle();" class="uiButton" >Create List</a>
</div>
<?php if(Yii::app()->user->hasFlash('addlist')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('addlist'); ?>
</div>
<?php else: 
endif; ?>
<div id="toggleText" style="display: none" class="displayText_add_list">
<?php $owner=Yii::app()->user->id;?>
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lists-addlist-form',
	'enableAjaxValidation'=>true,
)); ?>
<div id="inner">
<div class="pagearea ">
<div class="form ui-form formm">
	<div class="row">
		<?php echo $form->labelEx($model,'list_name'); ?>
		<?php echo $form->textField($model,'list_name'); ?>
		<?php echo $form->error($model,'list_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->hiddenfield($model,'owner_id',array('value'=>$owner)); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="row buttons button-container">
		<?php echo CHtml::submitButton('Create List',array('class'=>'uiButton ')); ?>
	</div>
</div>
</div>
</div>
</div>

<form action="#" id="delete_all" method="post">
<?php
print("Page no : ");
print_r(round($page));
?>
<div id="inner" >
	<div class="pagearea" id="Pagearea">
		<div>
			<div class="alpha_sort_list">
				<input type="checkbox" name="checkall" id = "checkall" class="chec_all" onclick = "check_all(); "/><b>Contact Detail</b>
				<?php echo CHtml::submitButton('Delete All',array('class'=>'uiButton ')); ?>
			</div>
		</div>
	<?php 

		if($dr==true) {
	?>
			<ul id="users" class="content_list">

	<?php 
	if(count($dr)<10){
	
			Show_Page_Content($dr,1,'lessthan10');
		
		}		
		else if($page > count($dr)/10){
			Show_Page_Content($dr,$page,'lastpage');
				
		}else{
			Show_Page_Content($dr,$page,'navigation');
		}
	
	}
	else{
		echo "No contacts found. Add your contacts "; echo CHtml::link(UserModule::t('Here'),array('/user/invite'));
	}
	?>
			</div>
			<div class="negtiveMargin"></div>
		</div>

<?php 
$count_con=count($dr);
echo CHtml::link(UserModule::t("<<  "),array('/user/invite/index','page'=>1));
if($page > 1){
echo CHtml::link(UserModule::t("Prev"),array('/user/invite/index','prev'=>$page-1));
	}
if($count_con <= 1)
{
	for($i=1;$i<=count($dr)/10;$i++)
		echo CHtml::link(UserModule::t("  ".$i."   "),array('/user/invite/index','page'=>$i));
}
else
{	
	if($page>5 ){

		if($page == round(count($dr)/10)){ 
			$page = round(count($dr)/10);
		
		 }
			for($i=$page;$i<=count($dr)/10;$i++)
			{		
			echo CHtml::link(UserModule::t("  ".$i."  "),array('/user/invite/index','page'=>$i));
			$count_page++;
				
				if($count_page>4){
						break;
				}
			}
		
	}else{
		for($i=1;$i<=count($dr)/10;$i++)
			{		
			echo CHtml::link(UserModule::t("  ".$i."  "),array('/user/invite/index','page'=>$i));
			$count_page++;
				if($count_page>4){
						break;
				}
			}
	}  	
	
} 
		if($page<round(count($dr)/10)){
		echo CHtml::link(UserModule::t("Next"),array('/user/invite/index','next'=>$page+1));
			}		
		echo CHtml::link(UserModule::t(" >>"),array('/user/invite/index','page'=>ceil(count($dr)/10)));	
?>
</form>

<div class="pagearea">
<div class="ui-form formm">
<h2>List Lists</h2>
		
	<?php 
		$owner=Yii::app()->user->id;

		$command=Yii::app()->db->createCommand();
		/*$command->select('*');
		$command->from('tbl_listadmins');
		$command->where("owner=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$listadmin=$datareader->readAll();print_r($listadmin);*/


		$command->select('l.list_id,l.list_name,l.description');
		$command->from('tbl_lists l');
		$command->join('tbl_listadmins a','a.list_id=l.list_id');
		$command->where("owner=:inviter and permission=1",array(":inviter"=>$owner));
		$datareader=$command->query(); 
		$dr1=$datareader->readAll();

		/*$command=Yii::app()->db->createCommand();
		$command->select('list_id,list_name,description,created_time');
		$command->from('tbl_lists');
		$command->where("list_id=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();*/
		
	?>
	<table>
	  <tr class="alpha_sort_list list_name_width">
	    <td><b>Listname</b></td>
	    <td><b>Description</b></td>
	    <td><b>Members</b></td>
	    <td><b>Functions</b></td>
	  </tr>
	<?php 
		if(!empty($dr1)) {
			foreach ($dr1 as $value)
			{ $members=$value['list_id'];
    			  $command=Yii::app()->db->createCommand();
			  $command->select('contact_id');
			  $command->from('tbl_list_contacts');
			  $command->where("list_id=:listid",array(":listid"=>$value['list_id']));
			  $datareader=$command->query();
			  $dr=$datareader->readAll();$count="(".count($dr).")"." "."Members";
			?>
				<tr>
				  <td><?php echo $value['list_name'] ; ?></td>
				  <td><?php echo $value['description'] ; ?></td>
				  <td><?php echo CHtml::link(UserModule::t($count),array('/user/list/members','id'=>$members)); ?></td>
				  <td style='text-align:center;'><?php if($owner==1){ echo CHtml::link(UserModule::t('Remove List'),array('/user/list/delete_list','id'=>$members)); ?> | <?php echo CHtml::link(UserModule::t('Manage List'),array('/user/list/managelist','id'=>$members)); } else{  echo CHtml::link(UserModule::t('Remove List'),array('/user/list/delete_list','id'=>$members)); }?></td>
				</tr>
			<?php			
			}
		}
		else{
			echo "No lists found. create a list ";
		}
	 ?>
	</table>
</div>
</div>
<?php $this->endWidget(); ?>
<?php
function Show_Page_Content($dr,$page,$from){


if($from =='lessthan10'){
	$start = 0;
	$condition = count($dr) ;
	}else if($from =='lastpage'){
	$start = ($page-1)*10 ;
	$condition = count($dr);	
	}else{
	$start = ($page-1)*10;	
	$condition=$page*10;	
}
	
for($index=$start;$index<$condition;$index++){
		 $countChkBx = $countChkBx +1;		
		?>
				<li class="stripe">
<div style="float:left;margin-left:200px;margin-right:-200px;">
<input type="checkbox" name="<?php echo 'ct_' ; echo $dr[$index]['contact_id'];?>" id="ck<?php echo $countChkBx;?>" class="chec" />
</div>
				<div class="item_actions" >
					<?php echo CHtml::link(UserModule::t('Delete'),array('/user/invite/delete_contact','id'=>$dr[$index]['contact_id']),array('class'=>'uiButton'),array("confirm"=>"Are you sure?")); ?> 
<?php echo CHtml::link(UserModule::t('Edit & Update'),array('/user/invite/update','id'=>$dr[$index]['contact_id']),array('class'=>'uiButton')); ?>
				</div>

 				<div class="preview_pic" >
<img alt='user' src=' <?php echo Yii::app()->request->baseUrl."/css/contacts_imgs/profile_blank_thumb.gif"; ?> ' />
					<div class="item_information">
						<b class="item_info"><?php echo $dr[$index]['contact_name']; ?></b>
						<div class="info_data">
							<span><?php echo CHtml::link(UserModule::t($dr[$index]['email']),array('/user/invite/view','id'=>$dr[$index]['contact_id'])); ?></a></span>
						</div>
					</div>
				</li>
		<?php			
		}
			
		 ?>
	<!--to get count of chkBox-->
	<input type = hidden id =chkBx_count value = "<?php echo($countChkBx);?>">
	<!--the above hidden to get count of chkBox -->
	</ul>

<?php

//function ends here
}
?>

<script type = "text/javascript">
//To make all the contacts selected when check all is clicked
function check_all(){
var i;
var chkall =document.getElementById('checkall');
var chkBx_count = document.getElementById('chkBx_count').value;
 
	if(chkall.checked == true){
		for (i=1;i<=chkBx_count;i++){
			document.getElementById('ck'+i).checked = true;
		}
	}else{
		for (i=1;i<=chkBx_count;i++){
			document.getElementById('ck'+i).checked = false;
		}
	}
}
</script>
