<div style="float:right;margin-top:-40px;">
	<?php echo CHtml::link(UserModule::t('Contacts'),array('/user/invite/index'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Add Contacts'),array('/user/invite'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import & Export'),array('/user/invite'),array( 'class'=>'uiButton')); ?>
</div>

<div class="pagearea ">
<div class="form ui-form formm">


<?php
$desc=$_POST['desc'];
$_SESSION['delimiter']=$_POST['delimiter'];
$delimiter=$_SESSION['delimiter'];
  // Configuration - Your Options
      $allowed_filetypes = array('.csv'); // These will be the types of file that will pass the validation.
      $max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
     
 
   $filename = $_FILES['userfile']['name']; // Get the name of the file (including file extension).
   $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); // Get the extension from the filename.


$myFile = $_FILES['userfile']['tmp_name'];
$fh = fopen($myFile, 'r');
$theData = fread($fh, filesize($myFile));
fclose($fh);

//$content= htmlspecialchars($theData);

//echo $theData;
   // Check if the filetype is allowed, if not DIE and inform the user.
   if(!in_array($ext,$allowed_filetypes))
      die('The file you attempted to upload is not allowed.');
 
   // Now check the filesize, if it is too large then DIE and inform the user.
   if(filesize($_FILES['userfile']['tmp_name']) > $max_filesize)
      die('The file you attempted to upload is too large.');
      else
         //echo 'There was an error during the file upload.  Please try again.'; // It failed :(. *


$content=csv_to_array($theData,$delimiter);

?>


<!--instead of str_getcsv-->
<?php
function convertcsv($input, $delimiter = ',', $enclosure = '"', $escape = '\\', $eol = '\n') {

        if (is_string($input) && !empty($input)) {
            $output = array();
            $tmp    = preg_split("/".$eol."/",$input);
            if (is_array($tmp) && !empty($tmp)) {
                while (list($line_num, $line) = each($tmp)) {
                    if (preg_match("/".$escape.$enclosure."/",$line)) {
                        while ($strlen = strlen($line)) {
                            $pos_delimiter       = strpos($line,$delimiter);
                            $pos_enclosure_start = strpos($line,$enclosure);
                            if (
                                is_int($pos_delimiter) && is_int($pos_enclosure_start)
                                && ($pos_enclosure_start < $pos_delimiter)
                                ) {
                                $enclosed_str = substr($line,1);
                                $pos_enclosure_end = strpos($enclosed_str,$enclosure);
                                $enclosed_str = substr($enclosed_str,0,$pos_enclosure_end);
                                $output[$line_num][] = $enclosed_str;
                                $offset = $pos_enclosure_end+3;
                            } else {
                                if (empty($pos_delimiter) && empty($pos_enclosure_start)) {
                                    $output[$line_num][] = substr($line,0);
                                    $offset = strlen($line);
                                } else {
                                    $output[$line_num][] = substr($line,0,$pos_delimiter);
                                    $offset = (
                                                !empty($pos_enclosure_start)
                                                && ($pos_enclosure_start < $pos_delimiter)
                                                )
                                                ?$pos_enclosure_start
                                                :$pos_delimiter+1;
                                }
                            }
                            $line = substr($line,$offset);
                        }
                    } else {
                        $line = preg_split("/".$delimiter."/",$line);
   
                        /*
                         * Validating against pesky extra line breaks creating false rows.
                         */
                        if (is_array($line) && !empty($line[0])) {
                            $output[$line_num] = $line;
                        } 
                    }
                }
                return $output;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } 
?>
<!--End-->

<?php
function csv_to_array($input, $delimiter='|')
{
    $header = null;
    $data = array();
    $csvData = convertcsv($input, "\n");
	$_SESSION['value']=$csvData;


    foreach($csvData as $csvLine1){
		foreach($csvLine1 as $csvLine){
        if(is_null($header)) $header = explode($delimiter, $csvLine);
        else{
           
            $items = explode($delimiter, $csvLine);
            for($n = 0, $m = count($header); $n < $m; $n++){
                $prepareData[$header[$n]] = $items[$n];
            }
                $data[] = $prepareData;
        }
    }
}
?> 
	<form name='frm_mapping' id ='frm_mapping' action="<?php echo Yii::app()->baseUrl;?>/index.php?r=user/invite/load_db" method='post'>
	<?php $count=0; 
		for($n=1,$m=11;$n < $m;$n++){ echo('<br />'); echo('<br />'); $count = $count + 1;?>
			
		<!--select for our db entity-->
			
			<select  name="t<?php echo($count);?>" id = "t<?php echo($count);?>" class ='uiButton' onchange = "GetValue(<?php echo($count);?>);">
				<option value ="zero">Choose the mapping Field</option>
			<?php foreach($header as $key=>$value){ ?>
				
				<option value ="<?php echo($value); ?>" ><?php echo($value); ?></option>
				
		<?php  } ?>
		</select>&nbsp &nbsp &nbsp
		<select  name="db<?php echo($count);?>" id="db<?php echo($count);?>" class ='uiButton'  >
			<option value ="zero">Choose the column</option>			
			<option value ="first_name">First Name</option>
			<option value ="last_name">Last Name</option>
			<option value ="dob">Date of Birth</option>
			<option value ="contact_name">Contact Name</option>
			<option value ="work_phone">Work Phone</option>
			<option value ="home">Home Number</option>
			<option value ="mobile_phone">Mobile Phone</option>
			<option value ="fax">Fax</option>
			<option value ="email">Email</option>
			<option value ="company">Company</option>
		</select>
	<?php } 
	?><br /><input class="uiButton " id="buttons_align" type='submit' value ='submit' onclick = "return value_validation(<?php echo($count);?>);"> 
	
		
	</form>
   <?php return $data;
} 
?>




<script type="text/javascript">
function value_validation(get_val){
 var i,j,k,l,m ;
var a= new Array(get_val);
var b= new Array(get_val);
var c= new Array(get_val);
var d= new Array(get_val);
var e= new Array(get_val);
var db1 = document.getElementById("db1").value;

if(db1 !='email'){

alert('First Column must be Email');
return false;
}

//for header entity  
for(i=1;i<=get_val;i++){
  a[i] = document.getElementById("t"+i).value;
	for(j=i+1;j<=get_val;j++){
		 b[j]=document.getElementById("t"+j).value;
		if(a[i] == b[j]){
			if(a[i] != 'zero'){
				alert(" Mapping can only be done against one paticular column");		
				return false;
			}
			
		} 
	}
}


//for db entity
for(k=1;k<=get_val;k++){
  c[k] = document.getElementById("db"+k).value;
	for(l=k+1;l<=get_val;l++){
		 d[l]=document.getElementById("db"+l).value;
		if(c[k] == d[l]){
			if(c[k] != 'zero'){
				alert("Same Column is mapped for two values");		
				return false;
			}
			
		} 
	}
}

for(m=1;m<=get_val;m++){
	e[m]=document.getElementById("db"+m).value;
	if(a[m]=='zero'){
		alert("All Columns Must be Mapped ");
		return false;
	}

}
return true; 


}
</script>
</div>
</div>
