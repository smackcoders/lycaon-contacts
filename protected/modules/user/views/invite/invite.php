<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Contacts'),array('/user/invite/index'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Add Contact'),array('/user/invite/addnew'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import from CSV'),array('/user/invite/index_csv'),array( 'class'=>'uiButton')); ?>
</div>
<?php
$this->breadcrumbs=array(
	'Import Contact',
);?>


<?php
require('openinviter/openinviter.php');
global $oi_services;
$inviter=new OpenInviter();
$oi_services=$inviter->getPlugins();
global $contents;
global $contents1;
$contents="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'><html>";
$contents.="<head>
<style type='text/css'>
body {
	background: #f0f0f0;
	margin: 0;
	padding: 0;
	font: 10px normal Verdana, Arial, Helvetica, sans-serif;
	color: #444;
}
h1 {font-size: 3em; margin: 20px 0;}
.container {width: 680px; margin: 10px auto;}
ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 32px;
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 100%;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 31px;
	line-height: 31px;
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px;
	background: #e0e0e0;
	overflow: hidden;
	position: relative;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: 1.2em;
	padding: 0 20px;
	border: 1px solid #fff;
	outline: none;
}
ul.tabs li a:hover {
	background: #ccc;
}	
html ul.tabs li.active, html ul.tabs li.active a:hover  {
	background: #fff;
	border-bottom: 1px solid #fff;
}
.tab_container {
	border: 1px solid #999;
	border-top: none;
	clear: both;
	float: left; 
	width: 100%;
	background: #fff;
	-moz-border-radius-bottomright: 5px;
	-khtml-border-radius-bottomright: 5px;
	-webkit-border-bottom-right-radius: 5px;
	-moz-border-radius-bottomleft: 5px;
	-khtml-border-radius-bottomleft: 5px;
	-webkit-border-bottom-left-radius: 5px;
}
.tab_content {
	padding: 20px;
	font-size: 1.2em;
}
.tab_content h2 {
	font-weight: normal;
	padding-bottom: 10px;
	border-bottom: 1px dashed #ddd;
	font-size: 1.8em;
}
.tab_content h3 a{
	color: #254588;
}
.tab_content img {
	float: left;
	margin: 0 20px 20px 0;
	border: 1px solid #ddd;
	padding: 5px;
}
</style>
<script type='text/javascript'
src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>
<script type='text/javascript'>
$(document).ready(function() 
	{
	$('.tab_content').hide();
	$('ul.tabs li:first').addClass('active').show();
	$('.tab_content:first').show();

	$('ul.tabs li').click(function() 
		{
		$('ul.tabs li').removeClass('active');
		$(this).addClass('active');
		$('.tab_content').hide();
		var activeTab = $(this).find('a').attr('href');
		$(activeTab).fadeIn();
		return false;
		});
	});
</script>




<script type='text/javascript' src='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/jquery.mousewheel-3.0.4.pack.js'></script>
<script type='text/javascript' src='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/jquery.fancybox-1.3.4.pack.js'></script>
<link rel='stylesheet' type='text/css' href='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/jquery.fancybox-1.3.4.css' media='screen' />
<link rel='stylesheet' href='".Yii::app()->request->baseUrl."/themes/normal/views/layouts/fancy-css/style.css'/>
<script type='text/javascript'>
$(document).ready(function() 
	{
";

$contents.="
});
</script>
</head>
<body><div class='container'>";

//$contents.=createHtmlServices('email');

$contents.="</body></html>";

//function createHtmlServices($type)
//	{
//	global $oi_services;
	//global $content1;
	$contents="";	$contents1="";
	$img1="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/gmail.png' alt='Gmail' height='60' width='130' style='padding-right:40px;' />";
	$img2="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/hotmail.png' alt='Hotmail' height='60' width='130' style='padding-right:40px;'/>";
	$img3="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/yahoo.png' alt='Yahoo' height='60' width='130' />";
	$img4="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/facebook.png' alt='facebook' height='60' width='130' style='padding-right:40px;' />";
	$img5="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/twitter.png' alt='twitter' height='40' width='130' style='padding-right:40px;margin-bottom:10px;'/>";
	$img6="<img src='".Yii::app()->request->baseUrl."/css/contacts_imgs/linkedin.png' alt='linkedin' height='60' width='130' />";
	$contents.=CHtml::Link($img1,array('/user/invite/get_contacts','provider_box'=>'gmail','type'=>'email'),array('id'=>'gmail'));
	$contents.=CHtml::Link($img2,array('/user/invite/get_contacts','provider_box'=>'hotmail','type'=>'email'),array('id'=>'hotmail'));
	$contents.=CHtml::Link($img3,array('/user/invite/get_contacts','provider_box'=>'yahoo','type'=>'email'),array('id'=>'yahoo'));
	$contents1.=CHtml::Link($img4,array('/user/invite/get_contacts','provider_box'=>'facebook','type'=>'social'),array('id'=>'facebook'));
	$contents1.=CHtml::Link($img5,array('/user/invite/get_contacts','provider_box'=>'twitter','type'=>'social'),array('id'=>'twitter'));
	$contents1.=CHtml::Link($img6,array('/user/invite/get_contacts','provider_box'=>'linkedin','type'=>'email'),array('id'=>'linkedin'));
?>
	
	<div>
	<?php
	$this->widget('zii.widgets.jui.CJuiAccordion',array('panels'=>array('Email Invites'=>$contents,'Social Invites'=>$contents1,),
		'options'=>array(
			'collapsible'=>false,
			'active'=>0,
			'autoHeight'=> false,	
			//'event'=>'mouseover',
		),
		'htmlOptions'=>array(
				'style'=>'width:730px;padding-left:90px;'
				
			),
		));

	?>
	</div>
	
<?php
		$contents.="</div>";
?>

