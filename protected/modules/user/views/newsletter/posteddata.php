<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'newsletter-posteddata-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php 
$owner=Yii::app()->user->id;
	foreach($_GET['id'] as $key=>$lists){
		if($lists=='on'){
			$list[]=$key;
		}		
	}
	if(count($list)!=0){
		$mails = array();
		foreach($list as $key=>$value){
			$command=Yii::app()->db->createCommand();
			$command->select('list_id');
			$command->from('tbl_lists');
			$command->where("list_name=:listname",array(":listname"=>$value));
			$datareader=$command->query();
			$r=$datareader->readAll();
			$command=Yii::app()->db->createCommand();
			$command->select('email,contact_name,l.subscription_key');
			$command->from('tbl_invites c');
			$command->join('tbl_list_contacts l','c.contact_id=l.contact_id');
			$command->where("list_id=:listid and subscription=1",array(":listid"=>$r[0]['list_id']));
			$datareader=$command->query();
			$dr1=$datareader->readAll();
			foreach($dr1 as $emails)
			{
				$mails[$emails['email']]=$emails['contact_name'].",".$emails['subscription_key'];
			}
		}

	}
	foreach($mails as $mail_key => $mail_value){
		$mail[]=$mail_key;
		$contact[]=$mail_value;
	}	
	if(isset($_GET['id']['Newsletter']['subject'])){
		$subject = $_GET['id']['Newsletter']['subject'] ;
	}
	else if(isset($_GET['id']['Savedmessages']['subject'])){
		$subject = $_GET['id']['Savedmessages']['subject'] ;
	}
	if(isset($_GET['id']['Newsletter']['from_name'])){
		$from_name = $_GET['id']['Newsletter']['from_name'] ;
	}
	else if(isset($_GET['id']['Savedmessages']['from_name'])){
		$from_name = $_GET['id']['Savedmessages']['from_name'] ;
	}
	if(isset($_GET['id']['Newsletter']['from_email'])){
		$from_email = $_GET['id']['Newsletter']['from_email'] ;
	}
	else if(isset($_GET['id']['Savedmessages']['from_email'])){
		$from_email = $_GET['id']['Savedmessages']['from_email'] ;
	}	
if(count($mail)!=0){
$i=0;
		$type=$_GET['id']['type'];
		if($type=='template'){
			while($i< count($mail)){
				$title=$_GET['id']['template'];
				$owner=Yii::app()->user->id;
				$command=Yii::app()->db->createCommand();
				$command->select('template');
				$command->from('tbl_template');
				$command->where("title=:title",array(":title"=>$title));
				$datareader=$command->query();
				$dr=$datareader->readAll();
				$var=stripslashes($dr[0]['template']);
				$var1=html_entity_decode($var,ENT_QUOTES);
				$cntct=explode(',',$contact[$i]);
				$subs_key=$cntct[1];
				$greetings="Dear ";
				if($cntct[0]!=''){
					$greetings.=$cntct[0];
				}
				else{
					$greetings.="Valid Subcriber";
				}

				$subcript_profile=$this->createAbsoluteUrl('/user/newsletter/subscriber_profile',array('key'=>$subs_key,'id'=>$mail[$i]));
                                $subscribtion="<a href='{$subcript_profile}'>Update your profile information | Safe Unsubscribe</a> ";
				$serverEpoch = date("M".','."d".','."g".':'."i"."a");
				$report = new Report;
				$report->subject=$subject;
				$report->sender_id=$owner;
				$report->sender_name=$from_name;
				$report->sender_mail=$from_email;
				$report->newsletter=$var;
				$report->scheduled_at=$serverEpoch;
				$report->receiver_mail=$mail[$i];
				$report->status_key=UserModule::encrypting(microtime().$mail[$i]);
				$report->save();
				$head=$this->createAbsoluteUrl('/user/newsletter/newspaper.png',array('key'=>$report->status_key));
				$header="<a href='{$head}'><img src='".Yii::app()->request->baseUrl."/images/newspaper.png' alt='newspaper' height='150' width='200'/></a>";
				$message = new YiiMailMessage;
				$message->setBody($header."<br/>".$greetings."<br/>".$var1."<br/>".$subscribtion."<br/>", 'text/html');
				$message->subject = $subject['subject'];
				$message->addTo($mail[$i]);
				$message->setFrom(array($from_email => $from_name));
				Yii::app()->mail->send($message);
			$i++;
			}
		}
		else{
			while($i< count($mail)){
				$msg='';
				$var='';
				$serverEpoch = date("M".','."d".','."g".':'."i"."a");
				$key=UserModule::encrypting(microtime().$mail[$i]);
				$owner=Yii::app()->user->id;
				$cntct=explode(',',$contact[$i]);
				$subs_key=$cntct[1];
				$greetings="Dear ";
				if($cntct[0]!=''){
					$greetings.=$cntct[0];
				}
				else{
					$greetings.="Valid Subcriber";
				}
				$subcript_profile=$this->createAbsoluteUrl('/user/newsletter/subscriber_profile',array('key'=>$subs_key,'id'=>$mail[$i]));
                                $subscribtion="<a href='{$subcript_profile}'>Update your profile information | Safe Unsubscribe</a> ";
				$var=stripslashes($_GET['id']['message']);
				$report = new Report;
				$report->subject=$subject;
				$report->sender_id=$owner;
				$report->sender_name=$from_name;
				$report->sender_mail=$from_email;
				$report->newsletter=$var;
				$report->scheduled_at=$serverEpoch;
				$report->receiver_mail=$mail[$i];
				$report->status_key=$key;
				$report->save();
				$command=Yii::app()->db->createCommand();
				$command->select('newsletter');
				$command->from('tbl_reports');
				$command->where("status_key=:key",array(":key"=>$key));
				$datareader=$command->query();
				$dr=$datareader->readAll();
				$msg=html_entity_decode($dr[0]['newsletter'],ENT_QUOTES);
				$head=$this->createAbsoluteUrl('/user/newsletter/newspaper.png',array('key'=>$key));
				$header="<a href='{$head}'><img src='".Yii::app()->request->baseUrl."/images/newspaper.png' alt='newspaper' height='150' width='200'/></a>";
				$message = new YiiMailMessage;
				$message->setBody($header."<br/>".$greetings."<br/>".$msg."<br/>".$subscribtion."<br/>", 'text/html');
				$message->subject = $subject;
				$message->addTo($mail[$i]);
				$message->setFrom(array($from_email => $from_name));
				Yii::app()->mail->send($message); 
			$i++;
			}
		}
}
?>

<?php $this->endWidget(); $this->redirect(array('index'));?>

</div><!-- form -->
