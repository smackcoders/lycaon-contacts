<?php $this->breadcrumbs=array(
	UserModule::t("Newsletter")=>array('index'),
	UserModule::t("Report"),
);
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Reports'),array('/user/newsletter/report'),array( 'class'=>'uiButton')); ?>
</div>
<div class="pagearea">
	<div class="ui-form formm">
		<h2>Newsletter Reports</h2>
		<table>
		  <tr class="alpha_sort_list list_name_width">
		    <th>Subject</th>
		    <th>Sender Name</th>
		    <th>Sender Mail</th>
		    <th>Scheduled At</th>
		    <th>Receiver Mail</th>
		    <th>Status</th>
		  </tr>
			<?php 
if($dr==true) {
	if(count($dr)<10){
		Show_Page_Content($dr,1,'lessthan10');
	}else if($page > count($dr)/10){
		Show_Page_Content($dr,$page,'lastpage');
	}else{
		Show_Page_Content($dr,$page,'navigation');
	}
$count_con=count($dr);
echo CHtml::link(UserModule::t("<<  "),array('/user/newsletter/report','page'=>1));
if($page > 1){
echo CHtml::link(UserModule::t("Prev"),array('/user/newsletter/report','prev'=>$page-1));
	}
if($count_con <= 1)
{
	for($i=1;$i<=count($dr)/10;$i++)
		echo CHtml::link(UserModule::t("  ".$i."   "),array('/user/newsletter/report','page'=>$i));
}
else
{	
	if($page>5 ){

		if($page == round(count($dr)/10)){ 
			$page = round(count($dr)/10);
		
		 }
			for($i=$page;$i<=count($dr)/10;$i++)
			{		
			echo CHtml::link(UserModule::t("  ".$i."  "),array('/user/newsletter/report','page'=>$i));
			$count_page++;
				
				if($count_page>4){
						break;
				}
			}
		
	}else{
		for($i=1;$i<=count($dr)/10;$i++)
			{		
			echo CHtml::link(UserModule::t("  ".$i."  "),array('/user/newsletter/report','page'=>$i));
			$count_page++;
				if($count_page>4){
						break;
				}
			}
	}  	
	
} 
		if($page<round(count($dr)/10)){
		echo CHtml::link(UserModule::t("Next"),array('/user/newsletter/report','next'=>$page+1));
			}		
		echo CHtml::link(UserModule::t(" >>"),array('/user/newsletter/report','page'=>ceil(count($dr)/10)));	
}
function Show_Page_Content($dr,$page,$from){
if($from =='lessthan10'){
	$start = 0;
	$condition = count($dr) ;
	}else if($from =='lastpage'){
	$start = ($page-1)*10 ;
	$condition = count($dr);	
	}else{
	$start = ($page-1)*10;	
	$condition=$page*10;	
}
	
for($index=$start;$index<$condition;$index++){
			if($dr[$index]['status']==0){
				$dr[$index]['status']='Pending';
			}
			else{
				$dr[$index]['status']='Viewed';
			}
			?>
		  <tr>
		   <td><?php echo $dr[$index]['subject'] ; ?></td>
		   <td><?php echo $dr[$index]['sender_name'] ; ?></td>
		   <td><?php echo $dr[$index]['sender_mail'] ; ?></td>
		   <td><?php echo $dr[$index]['scheduled_at'] ; ?></td>
		   <td><?php echo $dr[$index]['receiver_mail'] ; ?></td>
		   <td><?php echo $dr[$index]['status'] ; ?></td>
		  </tr>
			<?php }
	}
			?>
		</table>
	</div>
</div>
