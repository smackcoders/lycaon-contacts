<?php 
if(isset($_GET['id'])){
echo ("Am from view.php");
	$command=Yii::app()->db->createCommand();
	$command->select('subject,sender_name,sender_mail,newsletter');
	$command->from('tbl_reports');
	$command->where("report_id=:id",array(":id"=>$_GET['id']));
	$datareader=$command->query();
	$dr=$datareader->read();
	$msg=html_entity_decode($dr['newsletter'],ENT_QUOTES);
}
?>
<div class="form">
<?php $this->breadcrumbs=array(
	UserModule::t("Newsletter")=>array('index'),
	UserModule::t("Send Newsletter"),
);
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Reports'),array('/user/newsletter/report'),array( 'class'=>'uiButton')); ?>

</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'newsletter-send_newsletter-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="pagearea">
<div class="ui-form formm" id="send_newsletter_form">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($report); 
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('list_id,list_name');
		$command->from('tbl_lists');
		$command->where("owner_id=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr_list=$datareader->readAll();
	?>

	<div class="row">
		<?php echo $form->labelEx($report,'subject'); ?>
		<?php echo $form->textField($report,'subject',array('value'=>$dr['subject'])); ?>
		<?php echo $form->error($report,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($report,'sender_name'); ?>
		<?php echo $form->textField($report,'sender_name',array('value'=>$dr['sender_name'])); ?>
		<?php echo $form->error($report,'sender_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($report,'sender_mail'); ?>
		<?php echo $form->textField($report,'sender_mail',array('value'=>$dr['sender_mail'])); ?>
		<?php echo $form->error($report,'sender_mail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($report,'To list'); 
		foreach($dr_list as $listname){?>
		<input type="checkbox" name="<?php echo $listname['list_name']; ?>" id="list_id"/> <?php echo $listname['list_name']; ?>&nbsp;&nbsp;
		<?php } ?>
		<?php echo $form->error($report,'list_id'); ?>
	</div>

	<div class="row">
		<!--<?php echo $form->labelEx($report,'scheduled_at'); ?>-->
		<?php echo $form->hiddenField($report,'scheduled_at'); ?>
		<!--<?php echo $form->error($report,'scheduled_at'); ?>-->
	</div>

	<div id="editor" style="margin-left:140px;margin-top:60px;">
		<?php
		$this->widget('application.extensions.cleditor.ECLEditor', array(
			'name'=>'message','value'=>$msg,
		    ));
		?>
	</div>

	<div class="row buttons send_newsletter_button" style="float:right;margin-top:10px;">
		<?php echo CHtml::submitButton('Forward Newsletter',array('class'=>'uiButton')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
