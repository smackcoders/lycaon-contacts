<div class="form">
<?php $this->breadcrumbs=array(
	UserModule::t("Newsletter")=>array('index'),
	UserModule::t("Send Newsletter"),
);
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Reports'),array('/user/newsletter/report'),array( 'class'=>'uiButton')); ?>

</div>
<?php require_once (getcwd().'/protected/config/mail_conf.php');
global $ly_config;
	  if(!empty($ly_config["mail_host"]) && !empty($ly_config["port_no"]) && !empty($ly_config["mail_username"]) && !empty($ly_config["mail_password"]) && !empty($ly_config["enc"])){
 ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'newsletter-send_newsletter-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="pagearea">
<div class="ui-form formm" id="send_newsletter_form">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); 
		$owner=Yii::app()->user->id;

		$command=Yii::app()->db->createCommand();
		$command->select('list_id');
		$command->from('tbl_listadmins');
		$command->where("owner=:owner and permission=1",array(":owner"=>$owner));
		$datareader=$command->query();
		$list=$datareader->readAll();
		foreach($list as $li){
			$command=Yii::app()->db->createCommand();
			$command->select('list_name');
			$command->from('tbl_lists');
			$command->where("list_id=:listid",array(":listid"=>$li['list_id']));
			$datareader=$command->query();
			$dr_list[]=$datareader->readAll();
		}
		/*$command=Yii::app()->db->createCommand();
		$command->select('list_id,list_name');
		$command->from('tbl_lists');
		$command->where("owner_id=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr_list=$datareader->readAll();*/
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject'); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_name'); ?>
		<?php echo $form->textField($model,'from_name'); ?>
		<?php echo $form->error($model,'from_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_email'); ?>
		<?php echo $form->textField($model,'from_email'); ?>
		<?php echo $form->error($model,'from_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<input type="radio" onblur="makeDisable()" onclick="Test(this);" name="type" value="text"> Text
		<input type="radio" onblur="makeEnable()" onclick="Test(this);" name="type" value="template"> Template

		<?php
			$command=Yii::app()->db->createCommand();
			$command->select('title');
			$command->from('tbl_template');
			$command->where("owner_id=:owner",array(":owner"=>$owner));
			$datareader=$command->query();
			$dr=$datareader->readAll();
		?>

		<select disabled name="template" id="template" style="width:100px;" onchange="this.form.tname.value=dropdown_change(this.form.tname.value)">
			<option>--Choose--</option>	
		<?php

		foreach($dr as $key=>$title){ ?>
			<option><?php echo $title['title']; ?></option>
		<?php } ?>
		</select>
		<?php echo $form->error($model,'template_id'); ?>
		
		<?php echo $form->error($model,'type'); ?>
	</div>
	<?php if(count($dr_list)!=0){ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'list_id'); 

		foreach($dr_list as $listname){ ?>
		<input type="checkbox" name="<?php echo $listname[0]['list_name']; ?>" id="list_id"/> <?php echo $listname[0]['list_name']; ?>&nbsp;&nbsp;
		<?php } ?>
		<?php echo $form->error($model,'list_id'); ?>
	</div>
	<?php } ?>
	<div class="row">
		<!--<?php echo $form->labelEx($model,'scheduled_at'); ?>-->
		<?php echo $form->hiddenField($model,'scheduled_at'); ?>
		<!--<?php echo $form->error($model,'scheduled_at'); ?>-->
	</div>

	<div id="editor" style="display:none;margin-left:140px;margin-top:60px;">
		<?php
		$this->widget('application.extensions.cleditor.ECLEditor', array(
			'name'=>'message',
		    ));
		?>
	</div>

	<div class="row buttons send_newsletter_button" style="float:right;margin-top:10px;">
		<?php echo CHtml::submitButton('Save',array('class'=>'uiButton')); ?>
		<?php echo CHtml::submitButton('Send Newsletter',array('class'=>'uiButton')); ?>
	</div>
	<script type="text/javascript">
		function myPopup2() {
		window.open( "<?php echo Yii::app()->user->returnUrl; ?>?r=user/newsletter/preview", "myWindow", 
		"height = 500, width = 1000, resizable = 0" )
		}
	</script>
	<!--<div class="row buttons send_newsletter_button" style="float:right;">
		<?php echo CHtml::submitButton('Preview',array('onClick'=>'myPopup2()','class'=>'uiButton')); ?>
	</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php }
else{
  $this->redirect(array('profile/mail_settings'));
} ?>
</div>
