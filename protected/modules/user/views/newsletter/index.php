<?php $user_id=Yii::app()->user->id;
if(isset($user_id)){
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter' ),array( 'class'=>'uiButton')); ?>   <?php echo CHtml::link(UserModule::t('Write'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Drafts'),array('/user/newsletter/drafts'),array( 'class'=>'uiButton')); ?> <?php echo CHtml::link(UserModule::t('Send Messages'),array('/user/newsletter/report'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?> 
</div>
<?php $this->breadcrumbs=array(
	UserModule::t("Newsletter")=>array('index'),
	UserModule::t("Summary"),
);
?>
<div class="pagearea">
	<div class="ui-form formm">
		<h2>Newsletter Summary</h2>
		<table>
		  <tr class="alpha_sort_list list_name_width">
		    <th>Date Sent</th>
		    <th>Subject</th>
		    <th>Status</th>
		    <th>Action</th>
		  </tr>
			<?php 
			$owner=Yii::app()->user->id;
			$command=Yii::app()->db->createCommand();
			$command->select('report_id,subject,scheduled_at,status');
			$command->from('tbl_reports');
			$command->order('report_id desc');
			$command->limit(10);
			$command->where("sender_id=:owner",array(":owner"=>$owner));
			$datareader=$command->query();
			$dr=$datareader->readAll();
			foreach($dr as $report){
			if($report['status']==0){
				$report['status']='Pending';
			}
			else{
				$report['status']='Viewed';
			}
			?>
		  <tr>
		   <td><?php echo $report['scheduled_at'] ; ?></td>
		   <td><?php echo $report['subject'] ; ?></td>
		   <td><?php echo $report['status'] ; ?></td>
		   <td><?php echo CHtml::link(UserModule::t('Forward'),array('/user/newsletter/view','id'=>$report['report_id'])); ?></td>
		  </tr>
			<?php }
			?>
		</table>
	</div>
</div>
<?php }
else{
$this->redirect(array('/user/login'));
}
?>
