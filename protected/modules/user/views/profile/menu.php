<div class="actions" style="margin-bottom:35px;">
<?php 
if(UserModule::isAdmin() && UserModule::isAdminuser()) {
?>
<div style="float:right;margin-top:-100px;">
<?php echo CHtml::link(UserModule::t('Manage User'),array('/user/admin'),array( 'class'=>'uiButton')); ?>
<?php echo CHtml::link(UserModule::t('Edit Profile'),array('edit'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Change password'),array('changepassword'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Mail Settings'),array('mail_settings'),array( 'class'=>'uiButton')); ?>
</div>
<?php 
}else if(UserModule::isAdminuser()){
?>
<div style="float:right;margin-top:-100px;">
<!--<?php echo CHtml::link(UserModule::t('Create User'),array('/user/admin/create'),array( 'class'=>'uiButton')); ?>-->
<?php echo CHtml::link(UserModule::t('Edit Profile'),array('edit'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Change password'),array('changepassword'),array( 'class'=>'uiButton')); ?> <!--<?php echo CHtml::link(UserModule::t('Mail Settings'),array('mail_settings'),array( 'class'=>'uiButton')); ?>-->
</div>
<?php 
} else {
?>
<div style="float:right;margin-top:-100px;">
<?php echo CHtml::link(UserModule::t('Edit Profile'),array('edit'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Change password'),array('changepassword'),array( 'class'=>'uiButton')); ?>
</div>
<?php
}
?>

