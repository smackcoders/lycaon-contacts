<script type="text/javascript">
function notempty(){
	var mail_host= document.getElementById('mail_host').value.length;
	var enc= document.getElementById('enc').value.length;
	var mail_uname= document.getElementById('mail_uname').value.length;
	var mail_pwd= document.getElementById('mail_pwd').value.length;
	var port_no= document.getElementById('port_no').value.length;
	if((mail_host == 0)||(mail_uname == 0)||(enc == 0)||(port_no == 0)||(mail_pwd == 0)){
		if(mail_host == 0){
			document.getElementById('mail_host').style.borderColor="red";
			alert("Please Choose your Mail Host");
			setTimeout(function() {document.config.mail_host.focus()} ,10);
		}
		else if(port_no == 0){
			document.getElementById('port_no').style.borderColor="red";
			alert("Please enter your Port no");
			setTimeout(function() {document.config.port_no.focus()} ,10);
		}
		else if(mail_uname == 0){
			document.getElementById('mail_uname').style.borderColor="red";
			alert("Please enter your EMail-ID");
			setTimeout(function() {document.config.mail_uname.focus()} ,10);
		}
		else if(mail_pwd == 0){
			document.getElementById('mail_pwd').style.borderColor="red";
			alert("Please enter your Email Password");
			setTimeout(function() {document.config.mail_pwd.focus()} ,10);
		}
		else if(enc == 0){
			document.getElementById('enc').style.borderColor="red";
			alert("Please enter your encryption code");
			setTimeout(function() {document.config.enc.focus()} ,10);
		}
		return false;
	}
	else{
		return true;
	}
}
</script>

<?php if(isset($_POST['mail_host']) && isset($_POST['port_no']) && isset($_POST['mail_uname']) && isset($_POST['mail_pwd']) && isset($_POST['enc'])){
	 $string='<?php global $ly_config;$ly_config["mail_host"] ="' .$_POST['mail_host']. '";$ly_config["port_no"] ="' .$_POST['port_no']. '";$ly_config["mail_username"] ="' .$_POST['mail_uname']. '";$ly_config["mail_password"] ="' .$_POST['mail_pwd']. '";$ly_config["enc"] ="' .$_POST['enc']. '";?>';
		$fp = fopen(getcwd()."/protected/config/mail_conf.php", "w");

		fwrite($fp, $string);

		fclose($fp);
$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
$location= explode ('/',$pageURL);
$loc .=$location[0].'/'.$location[1]; ?>
<script type=text/javascript>
window.location = "http://<?php echo $loc;?>/index.php?r=user/newsletter/send_newsletter";
</script>
<?php	}
 ?>

<?php require_once(getcwd().'/protected/config/mail_conf.php');
global $ly_config;?>

<div class="form">
<?php $this->breadcrumbs=array(
	UserModule::t("Profile")=>array('profile'),
	UserModule::t("Mail Settings"),
);
?>
<div style="float:right;margin-top:-60px;">
	 <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?> 
</div>

<?php if(Yii::app()->user->hasFlash('mail_settings')): ?>
<div class="success" style="text-align:center;padding-bottom:10px;">
<?php echo Yii::app()->user->getFlash('mail_settings'); ?>
</div>
<?php else: 
endif; ?>

<div class="box">
	<form action="<?php echo Yii::app()->baseUrl; ?>/index.php?r=user/profile/mail_settings" method="post" name="config">
		<div class="install">
			<h1>Mail Configuration</h1>
			<table>
				<tr>
					<td>
						<label>Host <em>*</em></label>
					</td>
					<td>
						<input type="text" name="mail_host" id="mail_host" value="<?php echo($ly_config['mail_host']); ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Port No <em>*</em></label>
					</td>
					<td>
						<input type="text" name="port_no" id="port_no" value="<?php echo($ly_config['port_no']); ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Username <em>*</em></label>
					</td>
					<td>
						<input type="text" name="mail_uname" id="mail_uname" value="<?php echo($ly_config['mail_username']); ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Password <em>*</em></label>
					</td>
					<td>
						<input type="text" name="mail_pwd" id="mail_pwd" value="<?php echo($ly_config['mail_password']); ?>"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Encryption <em>*</em></label>
					</td>
					<td>
						<input type="text" name="enc" id="enc" value="<?php echo($ly_config['enc']); ?>"/>
					</td>
				</tr>		
			</table>
				<input type="submit" value="Configure" onClick="return notempty()" style="margin-left:45%;" class="uiButton"/>
		</div>

	</form>
</div>
</body>
</html>
</div>
