<?php
$this->breadcrumbs=array(
	'List',
);?>
<?php
$this->menu=array(
	array('label'=>'Contacts', 'url'=>array('/user/invite/index')),
	array('label'=>'Create List', 'url'=>array('/user/list/addlist')),
	array('label'=>'Manage Lists', 'url'=>array('admin')),
);
?>
<div id="inner">
<div class="pagearea">
<div class="ui-form formm">
	<?php 
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('list_id,list_name,description,created_time');
		$command->from('tbl_lists');
		$command->where("owner_id=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();
		if($dr==true) {
	?>
	<table>
	  <tr class="alpha_sort_list list_name_width">
	    <th>Listname</th>
	    <th>Description</th>
	    <th>Functions</th>
	  </tr>
	<?php 
		foreach ($dr as $value)
			{ $members=$value['list_id'];
			?>
				<tr>
				  <td><?php echo $value['list_name'] ; ?></td>
				  <td><?php echo $value['description'] ; ?></td>
				  <td><?php echo CHtml::link(UserModule::t('View Members'),array('/user/list/members','id'=>$members)); ?></td>
				</tr>
			<?php
			}
		}
		else{
			echo "No lists found. create a list ";echo CHtml::link(UserModule::t('Here'),array('/user/list/addlist'));
		}
	 ?>
	</table>
</div>
</div>
</div>

