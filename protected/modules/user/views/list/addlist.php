<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Add List");
$this->breadcrumbs=array(
	UserModule::t("List")=>array('list'),
	UserModule::t("Add List"),
);
?>
<?php
$this->menu=array(
	array('label'=>'Contacts', 'url'=>array('/user/invite/index')),
	array('label'=>'List Lists', 'url'=>array('index')),
	array('label'=>'Manage Lists', 'url'=>array('admin')),
);
?>
<?php if(Yii::app()->user->hasFlash('addlist')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('addlist'); ?>
</div>
<?php else: 
endif; ?>
<div id="inner">
<div class="pagearea ">
<div class="form ui-form formm">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lists-addlist-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<?php $owner=Yii::app()->user->id;?>
	<div class="row">
		<?php echo $form->labelEx($model,'list_name'); ?>
		<?php echo $form->textField($model,'list_name'); ?>
		<?php echo $form->error($model,'list_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->hiddenfield($model,'owner_id',array('value'=>$owner)); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>
-->
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
</div>
</div>
<h1 id="add_memb"><?php echo "Add Members";  ?></h1>
<?php
$this->menu=array(
	array('label'=>'Contacts', 'url'=>array('/user/invite/index')),
	array('label'=>'List Lists', 'url'=>array('index')),
	array('label'=>'Manage Lists', 'url'=>array('admin')),
);
?>
<div id="toggleText"  class="displayText_add_list">
<div style="margin-left:50px;">
	<?php 
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('c.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_invitedusers u','c.contact_id=u.contact_id');
		$command->where("inviter_id=:inviter",array(":inviter"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();
		if($dr==true) {
	?>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr class="box-body">
	    <th><input type="checkbox" name="<?php echo 'ct_' ; echo $value['contact_id']; ?>" id="contact" class="chec_content" /></th>
	    <th>Contact Name</th>
	    <th>Email ID</th>
	  </tr>

	<?php 
		foreach ($dr as $value)
		{ 
		?>
				<tr>
				<td><input type="checkbox" name="<?php echo 'ct_' ; echo $value['contact_id']; ?>" id="contact" class="chec_content" /></td>
<td>						<a class="item_info"><?php echo $value['contact_name'] ; ?></a></td>
<td>							<span><?php echo $value['email'] ; ?></span></td>
			</tr>
		<?php			
		}
	 ?>
	</table>
	<?php 
	}
	else{
		echo "No contacts found. Add your contacts "; echo CHtml::link(UserModule::t('Here'),array('/user/invite'));
	}
	?>
			</div>
			<div class="negtiveMargin"></div>
</div>
	<div class="button-container">
		<?php echo CHtml::submitButton('Create List',array('class'=>'uiButton')); ?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
