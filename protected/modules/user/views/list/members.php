<?php
$this->breadcrumbs=array(
	'List',
);
?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Add Contact'),array('/user/invite/addnew'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Import Contacts'),array('/user/invite'),array( 'class'=>'uiButton')); ?> 
</div>
<div class="pagearea ">
<div class="form ui-form formm">

<div>
<?php 
$owner=Yii::app()->user->id;
$listid=$_GET['id'];
	foreach($_POST as $key=>$value)
	{
		$val=substr($key,0,3);
	
		if($val =="ct_")
		{
			$contacts[]=$key;
			
		}
		else
		{
			//echo "U Got the output!";
		}
	
	}
	if(isset($contacts)){

		foreach($contacts as $cn){
			$cnts=substr($cn,3);
			$command=Yii::app()->db->createCommand();
			$command->select('contact_id');
			$command->from('tbl_list_contacts');
			$command->where("contact_id=:contact_id and list_id=:list_id",array(":contact_id"=>$cnts,":list_id"=>$_GET['id']));
			$datareader=$command->query();
			$dr=$datareader->read();
			if($dr['contact_id']!=$cnts){
				$command=Yii::app()->db->createCommand();
				$command->select('email');
				$command->from('tbl_invites');
				$command->where("contact_id=:contact",array(":contact"=>$cnts));
				$datareader=$command->query();
				$dr2=$datareader->read();
				$list_contacts= new List_contacts;
				$list_contacts->list_id=$_GET['id'];
				$list_contacts->contact_id=$cnts;
				$list_contacts->owner_id=$owner;
				$list_contacts->subscription_key=UserModule::encrypting(microtime().$dr2['email']);
				$list_contacts->save();
			}
		}
		$this->redirect(array('/user/list/members','id'=>$id));
	}
	else{
		//echo "Please select the contacts to add into list!.";
	}

	if($dr==true) {
	?>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr class="box-body">
	    <th>Contact Name</th>
	    <th>Email ID</th>
	    <th>Functions</th>
	  </tr>
		<?php 
			foreach ($dr as $value)
			{ 
			  $members=$value['contact_name'];
			  $mem_mail=$value['email'];
			?>
				<tr>
				  <td><?php echo $members ; ?></td>
				  <td><?php echo $mem_mail ; ?></td>
				  <td><?php echo CHtml::link(UserModule::t('Remove Member'),array('/user/list/delete','id'=>$listid,'contact_id'=>$value['contact_id']),array("confirm"=>"Are you sure?")); ?></td>
				</tr>
			<?php
			}
		}
		else{
			echo "No Members found!.";
		}
	 ?>
	</table>
</div>
</div>
</div>
<?php
echo CHtml::link(UserModule::t('<<'),array('/user/list/members','listpage'=>1,'con_page'=>$con_page+1,'id'=>$id));
$count=Yii::app()->session['count_contact_list'];
	if($_GET['listpage']>round($count/10)){
		echo CHtml::link(UserModule::t("Prev"),array('/user/list/members','listpage'=>$_GET['listpage']-1,'con_page'=>$con_page+1,'id'=>$id));
	}
	for($i=1;$i<($count/10)+1;$i++)
		echo CHtml::link(UserModule::t("  ".$i."   "),array('/user/list/members','listpage'=>$i,'con_page'=>$con_page+1,'id'=>$id));
	if($_GET['listpage']<round($count/10)){
		echo CHtml::link(UserModule::t("Next"),array('/user/list/members','listpage'=>$_GET['listpage']+1,'con_page'=>$con_page+1,'id'=>$id));
	}
echo CHtml::link(UserModule::t('>>'),array('/user/list/members','listpage'=>ceil($count/10),'con_page'=>$con_page+1,'id'=>$id));
?>
<h1><?php echo "Add Members";  ?></h1>

<form name="input" method="post">
<div style="margin-left:50px;">

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr class="box-body">
	    <th><input type="checkbox" name="<?php echo 'ct_' ; echo $value['contact_id']; ?>" id="contact" class="chec_content" onclick = "check_all();"/></th>
	    <th>Contact Name</th>
	    <th>Email ID</th>
	  </tr>
	<?php 
if($dr1==true) {
		foreach ($dr1 as $value)
		{ 
			$c= $c+1;
		?>

				<tr>
<td><input type="checkbox" name="<?php echo 'ct_' ; echo $value['contact_id']; ?>" id="contact<?php echo $c;?>" class="chec_content" /></td>
<td>						<a class="item_info"><?php echo $value['contact_name'] ; ?></a></td>
<td>							<span><?php echo $value['email'] ; ?></span></td>
			</tr>

		<?php
		}
	 ?><input type = hidden id =chkBx_count value = "<?php echo($c);?>">

	<?php 
	}
	else{
		echo "No contacts found. Add your contacts "; echo CHtml::link(UserModule::t('Here'),array('/user/invite'));
	}
	?>
	</table>
	</div>
	<div class="negtiveMargin"></div><br/>

	<div >
			<?php echo CHtml::submitButton(UserModule::t("Group contacts"),array('class'=>'uiButton' ,'id'=>'buttons_align')); ?>
	</div>
<?php
echo CHtml::link(UserModule::t('<<'),array('/user/list/members','listpage'=>$listpage+1,'con_page'=>1,'id'=>$id));
$count=Yii::app()->session['count_contacts'];
	if($con_page+1>1){
		echo CHtml::link(UserModule::t("Prev"),array('/user/list/members','listpage'=>$listpage+1,'con_page'=>$con_page,'id'=>$id));
	}
	for($i=1;$i<=($count/10)+1;$i++)
		echo CHtml::link(UserModule::t("  ".$i."   "),array('/user/list/members','listpage'=>$listpage+1,'con_page'=>$i,'id'=>$id));
	if($con_page+1<round($count/10)){
		echo CHtml::link(UserModule::t("Next"),array('/user/list/members','listpage'=>$listpage+1,'con_page'=>$con_page+2,'id'=>$id));
	}
echo CHtml::link(UserModule::t('>>'),array('/user/list/members','listpage'=>$listpage+1,'con_page'=>ceil($count/10),'id'=>$id));
?>
</form>

<script type = "text/javascript">
//To make all the contacts selected when check all is clicked
function check_all(){
var i;
var chkall =document.getElementById('contact');
var chkBx_count = document.getElementById('chkBx_count').value;
 
	if(chkall.checked == true){
		for (i=1;i<=chkBx_count;i++){
			document.getElementById('contact'+i).checked = true;
		}
	}else{
		for (i=1;i<=chkBx_count;i++){
			document.getElementById('contact'+i).checked = false;
		}
	}
}
</script>
