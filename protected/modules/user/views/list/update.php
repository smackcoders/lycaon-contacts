<?php
$this->breadcrumbs=array(
	'Lists'=>array('index'),
	$model->list_id=>array('view','id'=>$model->list_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Lists', 'url'=>array('index')),
	array('label'=>'Create Lists', 'url'=>array('create')),
	array('label'=>'View Lists', 'url'=>array('view', 'id'=>$model->list_id)),
	array('label'=>'Manage Lists', 'url'=>array('admin')),
);
?>

<h1>Update List(<?php echo $model->list_name; ?>)</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
