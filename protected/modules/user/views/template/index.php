<?php
$this->breadcrumbs=array(
	'Template',
);?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Reports'),array('/user/newsletter/report'),array( 'class'=>'uiButton')); ?>

</div>
<?php
$this->menu=array(
	array('label'=>'Send Newsletter', 'url'=>array('/user/newsletter/send_newsletter')),
);
?>
<h2>Upload Your Template!</h2>
<?php if(Yii::app()->user->hasFlash('index')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('index'); ?>
</div>
<?php else: 
endif; ?>

<div class="pagearea">
<div class="ui-form formm">

<form action="<?php echo Yii::app()->baseUrl; ?>/index.php?r=user/template/upload" method="post" enctype="multipart/form-data">
   <p>
	 <label> Template Title :</label>
    <input type="textbox" name="desc"/>   </br></br>
      <label for="file">Select a file:</label> <input type="file" name="userfile" id="file"> <br />
      <button class="uiButton" id="template_index_button">Upload File</button>
   <p>
</form>
</div>
</div>
<div class="pagearea">
<div class="ui-form formm">
<?php
$owner=Yii::app()->user->id;
$command=Yii::app()->db->createCommand();
$command->select('template_id,title,template');
$command->from('tbl_template');
$command->where("owner_id=:owner",array(":owner"=>$owner));
$datareader=$command->query();
$dr=$datareader->readAll();
if($dr){
$var=stripslashes($dr[0]['template']);
$var1=html_entity_decode($var,ENT_QUOTES);
}
?>

<table>
 <tr class="alpha_sort_list list_name_width">
  <th>Existing templates</th>
  <th>Delete</th>
  <th>View</th>
 </tr>


<?php if(count($dr)!=0){
	foreach($dr as $key=>$value){ ?>
	<tr>
	<td><?php echo ($value['title']); ?></td>
	<td><?php echo CHtml::link(UserModule::t('Delete'),array('/user/template/delete','id'=>$value['template_id']),array("confirm"=>"Are you sure?")); ?></td>
	<td><?php echo CHtml::link(UserModule::t('View & Edit'),array('/user/template/view','id'=>$value['template_id'])); ?></td>
	</tr>
<?php
	}
      } 
	else{ ?>
	<tr>
	 <td>
		<?php echo "No Templates found!."; ?>
	 </td>
	</tr>
<?php	} 
?>
</table>
</div>
</div>

