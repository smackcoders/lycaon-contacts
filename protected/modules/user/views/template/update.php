<?php 
	$value=$_POST['field'];
	$title=$_POST['title'];
	$t_id=$_POST['t_id'];
	$content= htmlspecialchars($value);
	if(isset($t_id)){
		$model = Template::model()->findByPk($t_id);
		$model->template = $content;
		$model->title = $title;
		$model->save();
		Yii::app()->user->setFlash('index',UserModule::t("Your Template is successfully updated!."));
	}
$this->redirect(array('index'));
?>

