<?php
$this->breadcrumbs=array(
	UserModule::t("Template")=>array('index'),
	UserModule::t("View Template"),
);?>
<div style="float:right;margin-top:-60px;">
	<?php echo CHtml::link(UserModule::t('Summary'),array('/user/newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Send Newsletter'),array('/user/newsletter/send_newsletter'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Templates'),array('/user/template'),array( 'class'=>'uiButton')); ?>  <?php echo CHtml::link(UserModule::t('Reports'),array('/user/newsletter/reports'),array( 'class'=>'uiButton')); ?>
</div>
<?php
$this->menu=array(
	array('label'=>'Send Newsletter', 'url'=>array('/user/newsletter/send_newsletter')),
);
?>
<?php
	if(isset($_GET['id'])){
		$t_id=$_GET['id'];
		$command=Yii::app()->db->createCommand();
		$command->select('title,template');
		$command->from('tbl_template');
		$command->where("template_id=:t_id",array(":t_id"=>$t_id));
		$datareader=$command->query();
		$dr=$datareader->readAll();
		$var=stripslashes($dr[0]['template']);
		$var1=html_entity_decode($var,ENT_QUOTES);
	}
?>
<div class="pagearea">
<div class="ui-form formm">
<form method="post" action="<?php echo Yii::app()->baseUrl; ?>/index.php?r=user/template/update">
	<label>Title :</label>
	<input type="textbox" value="<?php print($dr[0]['title']); ?>" name="title" /></br></br>
	<label>Template :</label></br></br>
	<div style="margin-left:140px;">
		<?php
		$this->widget('application.extensions.cleditor.ECLEditor', array(
			'name'=>'field','value'=>$var1,
		    ));
		?>
	</div>
	<input type="hidden" value="<?php print($t_id); ?>" name="t_id"/>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Update',array('class'=>'uiButton' , 'id'=>'buttons_align')); ?>
	</div>
</form>

</div>
</div>

