<div class="form">

<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
<br/>
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo CHtml::errorSummary(array($model,$profile)); ?>
<div class="pagearea prof_pag">
<table class="dataGrid content_list ui-form prof_page" >
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'username'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
	</th>
	<td>
		<?php echo CHtml::activePasswordField($model,'password',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'password'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'email'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'email',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'email'); ?>
	</tr>
	<!--<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'superuser'); ?>
	</th>
	<td>
		<?php echo CHtml::activeDropDownList($model,'superuser',User::itemAlias('AdminStatus')); ?>
	</td>
		<?php echo CHtml::error($model,'superuser'); ?>
	</tr>
	<tr>-->
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'adminuser'); ?>
	</th>
	<td>
		<?php echo CHtml::activeDropDownList($model,'adminuser',User::itemAlias('AdminStatus')); ?>
	</td>
		<?php echo CHtml::error($model,'adminuser'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'status'); ?>
	</th>
	<td>
		<?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus')); ?>
	</td>
		<?php echo CHtml::error($model,'status'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'contact_no'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'contact_no',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'contact_no'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'org_name'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'org_name',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'org_name'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'org_desc'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'org_desc',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'org_desc'); ?>
	</tr> 
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'country'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'country',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'country'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'state'); ?>
	</th>	
	<td>
		<?php echo CHtml::activeTextField($model,'state',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'state'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'city'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'city',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'city'); ?>
	</tr>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($model,'website'); ?>
	</th>
	<td>
		<?php echo CHtml::activeTextField($model,'website',array('class'=>'textfield')); ?>
	</td>
		<?php echo CHtml::error($model,'website'); ?>
	</tr>







<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<tr>
	<th class="label">
		<?php echo CHtml::activeLabelEx($profile,$field->varname); ?>
	</th>
	<td>
		<?php 
		if ($field->widgetEdit($profile)) {
			echo $field->widgetEdit($profile);
		} elseif ($field->range) {
			echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo CHtml::activeTextField($profile,$field->varname,array('class'=>'textfield'));
		}
		 ?>
		<?php echo CHtml::error($profile,$field->varname); ?>
	</td>
	</tr>	
			<?php
			}
		}
?>
	<tr>
	<td colspan="2" style="padding-left:400px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create User' : 'Save',array('class'=>'uiButton')); ?>
	</td>
	</tr>
</table>
</div>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->
