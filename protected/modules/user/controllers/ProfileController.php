<?php

class ProfileController extends Controller
{
	public $defaultAction = 'profile';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
		$model = $this->loadUser();
	    $this->render('profile',array(
	    	'model'=>$model,
			'profile'=>$model->profile,
	    ));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionMail_settings()
	{
		$command=Yii::app()->db->createCommand();
		$command->select('count(*)');
		$command->from('mail_conf');
		$datareader=$command->query();
		$count=$datareader->readAll();
	   
	   if(isset($_POST['mail_host']) && isset($_POST['port_no']) && isset($_POST['mail_uname']) && isset($_POST['mail_pwd']) && isset($_POST['enc'])){
		if($count[0]['count(*)']==0){
			$command->insert('mail_conf', array('host_name'=>$_POST['mail_host'],'port_no'=>$_POST['port_no'],'username'=>$_POST['mail_uname'],'password'=>$_POST['mail_pwd'],'encrypt_key'=>$_POST['enc']));
		}
		else{
			$command->update('mail_conf',array('host_name'=>$_POST['mail_host'],'port_no'=>$_POST['port_no'],'username'=>$_POST['mail_uname'],'password'=>$_POST['mail_pwd'],'encrypt_key'=>$_POST['enc']));
		}
		Yii::app()->user->setFlash('mail_settings',UserModule::t("Your mail settings is successfully Configured!"));
	   }
		$this->render('mail_settings');
	}

	public function actionEdit()
	{
		$model = $this->loadUser();
		$profile=$model->profile;
	
	// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}
		
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$profile->attributes=$_POST['Profile'];
			
			if($model->validate()&&$profile->validate()) {
				$owner=Yii::app()->user->id;
				$command=Yii::app()->db->createCommand();
				$command->update('tbl_users', array('org_name'=>$_POST['User']['org_name'],'org_desc'=>$_POST['User']['org_desc'],'city'=>$_POST['User']['city'],'country'=>$_POST['User']['country'],'state'=>$_POST['User']['state'],'contact_no'=>$_POST['User']['contact_no'],'website'=>$_POST['User']['website'],), 'id=:id', array(':id'=>$owner));
				$command->reset();
				$command->update('tbl_profiles',array('firstname'=>$_POST['Profile']['firstname'],'lastname'=>$_POST['Profile']['lastname'],'birthday'=>$_POST['Profile']['birthday'],),'user_id=:user_id',array(':user_id'=>$owner)); 				
				$command=Yii::app()->db->createCommand();
				$command->select('*');
				$command->from('tbl_users');
				$command->where("id=:id",array(":id"=>yii::app()->user->id));
				$datareader=$command->query();
				$drU=$datareader->readAll();
				$countU=0;
				$countP=0;
				$countT=0;
				foreach($drU as $keyU=>$valueU)	{
					foreach($valueU as $key1U=>$value1U) {
						$countT++;
						if($valueU[$key1U] == '') {
							$countU++;
						}	
					}
				}
				$command->reset();
				$command->select('*');
				$command->from('tbl_profiles');
				$command->where("user_id=:user_id",array(":user_id"=>yii::app()->user->id));
				$datareader=$command->query();
				$drP=$datareader->readAll();
				foreach($drP as $keyP=>$valueP ) {
					foreach($valueP as $key1P=>$value1P) {
						$countT++;
						if($valueP[$key1P] == '') {
							$countP++;
						}	
					}				
				}
				$total= (($countP + $countU) / $countT) * 100;
				$_SESSION['tofill']=$total;
		Yii::app()->user->setFlash('profileMessage',UserModule::t("Changes is saved."));
		$this->redirect(array('/user/profile'));
			} else $profile->validate();
		}
			
		$this->render('edit',array(
			'model'=>$model,
			'profile'=>$profile,
		));

						
	}
	
	/**
	 * Change password
	 */
	public function actionChangepassword() {
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {
			
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='changepassword-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}
			
			if(isset($_POST['UserChangePassword'])) {
					$model->attributes=$_POST['UserChangePassword'];
					if($model->validate()) {
						$new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
						$new_password->password = UserModule::encrypting($model->password);
						$new_password->activkey=UserModule::encrypting(microtime().$model->password);
						$new_password->save();
						Yii::app()->user->setFlash('profileMessage',UserModule::t("New password is saved."));
						$this->redirect(array("profile"));
					}
			}
			$this->render('changepassword',array('model'=>$model));
	    }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
			
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
}
