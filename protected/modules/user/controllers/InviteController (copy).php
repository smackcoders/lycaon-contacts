<?php

class InviteController extends Controller
{
	/*public function actionIndex()
	{
		$this->render('index');
	}*/

public $defaultAction = 'invite';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('invite','get_contacts'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionInvite()
	{	
		$this->render('invite');
	}

	public function actionGet_contacts($provider_box,$type)
	{
		/*foreach ($_POST as $key=>$val)
			if (strpos($key,'check_')!==false)
				$selected_contacts[$_POST['email_'.$val]]=$_POST['name_'.$val];
			elseif (strpos($key,'email_')!==false)
			{
				$temp=explode('_',$key);$counter=$temp[1];
			if (is_numeric($temp[1])) $contacts[$val]=$_POST['name_'.$temp[1]];
			}

		/*if (!empty($_POST['step']))$step=$_POST['step'];
		else $step='get_contacts';
		if ($step=='send_invites')
		{
			if(isset($_POST['contacts']))
			{	
				
				foreach($_POST['contacts'] as $email=>$name)
				{				
					$user=User::model()->findByAttributes(array('email'=>$email));
					if($user==null)
					{
						$invite=Invite::model()->findByAttributes(array('contact_id'=>$email));
						if($invite==NULL)
						{
								$command=Yii::app()->db->createCommand();
								$command->select('id');
								$command->from('tbl_invites');
								$command->where("email_service=:name",array("name"=>$provider_box));
								$datareader=$command->query();								
								if(($dr=$datareader->read())==true)
								{
									$serve_id=$dr['id'];
								}
								$inviter= new Invite;			
							$inviter->invited=0;
							$inviter->is_member=0;
							$inviter->user_id=Yii::app()->user->id;
							$inviter->contact_id=$email;
							$inviter->contact_name=$name;
							$inviter->inviter_id=$serve_id;
							$inviter->save();
								$command=Yii::app()->db->createCommand();
								$command->select('id');
								$command->from('tbl_invites');
								$command->order("id DESC");
								$command->limit(1);
								$datareader=$command->query();
								if(($dr=$datareader->read())==true)
								{
									$invit_id=$dr['id'];
								}
								$black=new InviterBlacklist;
								$black->invite_id=$invit_id;
								$black->unsubscribed=0;
								$black->save();
						}
						else
						{
								$command=Yii::app()->db->createCommand();
								$command->select('id');
								$command->from('tbl_invites');
								$command->where("email_service=:name",array(":name"=>$provider_box));
								$datareader=$command->query();
								if(($dr=$datareader->read())==true)
								{
									$serve_id=$dr['id'];
								}
								$inviter= new Invite;	
							$command=Yii::app()->db->createCommand();
							$command->select('id');
							$command->from('tbl_invites');
							$command->where("contact_id=:id",array(':id'=>$email));
							$datareader=$command->query();
							while(($dr=$datareader->read())==true)
							{
								$invite_id[]=$dr['id'];
							}
							$flag=0;
							$i=0;
							$count1=count($invite_id);
							while($i<$count1)
							{
								$blacklist=InviterBlacklist::model()->findByAttributes(array('inviter_id'=>$invite_id[$i]));
								if($blacklist->unsubscribed==1)
								$flag=1;
								$i++;
							}
							if($flag==0)
							{
								$inviter->invited=1;
								$inviter->is_member=0;
								$inviter->user_id=Yii::app()->user->id;
								$inviter->contact_id=$email;
								$inviter->contact_name=$name;
								$inviter->inviter_id=$serve_id;
								$inviter->save();
									$command=Yii::app()->db->createCommand();
									$command->select('id');
									$command->from('tbl_invites');
									$command->order("id DESC");
									$command->limit(1);
									$datareader=$command->query();
									if(($dr=$datareader->read())==true)
									{
										$invit_id=$dr['id'];
									}
									$black=new InviterBlacklist;
									$black->invite_id=$invit_id;
									$black->unsubscribed=0;
									$black->save();
									
							}
						}$invite=null;
					}
				}
			}
		}*/
		$this->render('get_contacts',array('provider_box'=>$provider_box));
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	/*public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}

	public function loadModel($id)
	{
		$model=Invite::model()->findByPk($id);
		if($model===null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}

	protected function performAjaxValidation($model)
	{
	    if(isset($_POST['ajax']) && $_POST['ajax']==='account-form')
	    {
		echo CActiveForm::validate($model);
		Yii::app()->end();
	    }
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
