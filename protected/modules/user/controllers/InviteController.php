<?php

class InviteController extends Controller
{

public $defaultAction = 'invite';
//public $layout='//layouts/column2';
	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('invite','get_contacts','index','group_contacts','addnew','admin','update','view','delete_contact','index_csv','load_db','upload_csv','invites_export'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionIndex()
	{
		if(isset($_POST['yt1'])){
                       foreach($_POST as $key=>$value){
                               if(($value=='on')&&($key!='checkall')){
                                       $val=substr($key,3,7);
                                       $command=Yii::app()->db->createCommand();
                                       $inviter=Yii::app()->user->id;
                                       //$command->delete('tbl_invitedusers','inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$val));
				       $command->update('tbl_invitedusers',array('deleted'=>1),'inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$val));
                                       $datareader=$command->execute();
					$command=Yii::app()->db->createCommand();
					$command->select('list_id,contact_id');
					$command->from('tbl_list_contacts');
					$command->where("owner_id=:owner",array(":owner"=>$inviter));
					$datareader=$command->query();
					$dr=$datareader->readAll();//print_r($dr);die;
					foreach($dr as $key1=>$value1){
						$arr[]=$value1['list_id'];
						if(isset($value1['contact_id'])){
							if($value1['contact_id']==$val){
								$command=Yii::app()->db->createCommand();
								$command->delete('tbl_list_contacts','contact_id=:contact and owner_id=:inviter', array(":contact"=>$val,":inviter"=>$inviter));
								$datareader=$command->execute();	
							}
						}
					}
                               }
                       }
		}
		$model=new Lists;
		 if(isset($_POST['ajax']) && $_POST['ajax']==='lists-addlist-form')
	    {
		echo UActiveForm::validate(array($model,$profile));
		Yii::app()->end();
            }
	    if(isset($_POST['Lists']))
	    {
		$model->attributes=$_POST['Lists'];
		if($model->validate())
		{
			
			foreach($_POST as $key=>$value)
			{
				$val=substr($key,0,3);	
				if($val =="ct_")
				{
					$contacts[]=$key;
				}
				else
				{
					//echo "U Got the output!";
				}
	
			}
			if(isset($contacts)){
				foreach($contacts as $cn){
					$cts[]=substr($cn,3);
				}
				//print_r($cts);
		   	// form inputs are valid, do something here
				$owner=Yii::app()->user->id;
				$list_name=$_REQUEST['Lists']['list_name'];
				$list_desc=$_REQUEST['Lists']['description'];
				$command=Yii::app()->db->createCommand();
				$command->select('list_name');
				$command->from('tbl_lists');
				$command->where("list_name=:l_name",array(":l_name"=>$list_name));
				$datareader=$command->query();
				$dr=$datareader->readAll();
				if($dr!=true)
				{
					$list= new Lists;	
					$list->list_name=$list_name;
					$list->description=$list_desc;
					$list->created_time=time();
					$list->owner_id=$owner;
					$list->save();
					$command=Yii::app()->db->createCommand();
					$command->select('list_id');
					$command->from('tbl_lists');
					$command->where("list_name=:listname",array(":listname"=>$_POST['Lists']['list_name']));
					$datareader=$command->query();
					$dr1=$datareader->read();
					if(isset($dr1['list_id'])){
						foreach($cts as $key=>$value){
							$command=Yii::app()->db->createCommand();
							$command->select('email');
							$command->from('tbl_invites');
							$command->where("contact_id=:contact",array(":contact"=>$value));
							$datareader=$command->query();
							$dr2=$datareader->read();
							$list_contacts= new List_contacts;
							$list_contacts->list_id=$dr1['list_id'];
							$list_contacts->contact_id=$value;
							$list_contacts->owner_id=$owner;
							$list_contacts->subscription_key=UserModule::encrypting(microtime().$dr2['email']);
							$list_contacts->save();
						}
						$list= new List_admins;	
						$list->list_id=$dr1['list_id'];
						$list->owner=$owner;
						$list->save();
					}
					Yii::app()->user->setFlash('addlist',UserModule::t("Thank you! Your list is successfully created!."));
					$this->redirect(array('index'));
				}
				else
				{
			
					Yii::app()->user->setFlash('addlist',UserModule::t("The List name is already present.Please choose another list name!."));
				}
				Yii::app()->user->setFlash('addlist',UserModule::t("The List name is already present.Please give another name!."));				
			}
			else{
				Yii::app()->user->setFlash('addlist',UserModule::t("Please select contacts while creating a new list!."));
			}
		    //return;
		}
		
	    }
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('c.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_invitedusers u','c.contact_id=u.contact_id');
		$command->where("inviter_id=:inviter and deleted=0",array(":inviter"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();
	
		if(isset($_GET['page']))
		{
			$this->render('index',array('model'=>$model,'page'=>$_GET['page'],'dr'=>$dr));
		}
		else if(isset($_GET['next']))
		{
			$this->render('index',array('model'=>$model,'page'=>$_GET['next'],'dr'=>$dr));
		}
		else if(isset($_GET['prev']))
		{
			$this->render('index',array('model'=>$model,'page'=>$_GET['prev'],'dr'=>$dr));
		}
		else
		{
			$this->render('index',array('model'=>$model,'page'=>1,'dr'=>$dr));
		}
		
	}

	
	public function actionGroup_contacts()
	{
		$this->render('group_contacts');	
	}
//***for uploading csv
	public function actionLoad_db()
	{

		$this->render('load_db');
	}
	public function actionIndex_csv()
	{
		$this->render('index_csv');
	}
	
	public function actionInvites_export()
	{
		$this->renderpartial('invites_export');
	}

	public function actionUpload_csv()
	{
		$this->render('upload_csv');
	}

	public function actionDelete_contact()
	{
		$this->render('delete_contact');	
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invite']))
		{
			$model->attributes=$_POST['Invite'];
			$model->first_name=$_POST['Invite']['first_name'];
			$model->last_name=$_POST['Invite']['last_name'];
			$model->dob=$_POST['dob'];
			$model->contact_name=$_POST['Invite']['first_name']." ".$_POST['Invite']['last_name'];
			$model->work_phone=$_POST['Invite']['work_phone'];
			$model->home=$_POST['Invite']['home'];
			$model->mobile_phone=$_POST['Invite']['mobile_phone'];
			$model->fax=$_POST['Invite']['fax'];
			$model->email=$_POST['Invite']['email'];
			$model->company=$_POST['Invite']['company'];
			$model->save();
			$this->redirect(array('view','id'=>$model->contact_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionInvite()
	{	
		$this->render('invite');
	}
	
	public function actionAdmin()
	{
		$model=new Invite('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invite']))
			$model->attributes=$_GET['Invite'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionAddnew()
	{
    		$model=new Invite;

	    // uncomment the following code to enable ajax-based validation
	    /*
	    if(isset($_POST['ajax']) && $_POST['ajax']==='invite-addnew-form')
	    {
		echo CActiveForm::validate($model);
		Yii::app()->end();
	    }
	    */
		
	   if(isset($_POST['Invite']))
	    { 
		$model->attributes=$_POST['Invite'];
		if($model->validate())
		{
			if(isset($_POST['yt1'])){
			$command=Yii::app()->db->createCommand();
			$command->select('contact_id');
			$command->from('tbl_invites');
			$command->where("email=:email",array(":email"=>$_POST['Invite']['email']));
			$datareader=$command->query();
			$dr=$datareader->read();$contact_id=$dr['contact_id'];
			$inviter= new Invite;
			if($dr!=true){
				$inviter->first_name=$_POST['Invite']['first_name'];
				$inviter->last_name=$_POST['Invite']['last_name'];
				$inviter->dob=$_POST['dob'];
				$inviter->contact_name=$_POST['Invite']['first_name']." ".$_POST['Invite']['last_name'];
				$inviter->work_phone=$_POST['Invite']['work_phone'];
				$inviter->home=$_POST['Invite']['home'];
				$inviter->mobile_phone=$_POST['Invite']['mobile_phone'];
				$inviter->fax=$_POST['Invite']['fax'];
				$inviter->email=$_POST['Invite']['email'];
				$inviter->company=$_POST['Invite']['company'];
				$inviter->save();
				$invite_user= new Invitedusers;
				$invite_user->inviter_id=Yii::app()->user->id;
				$invite_user->contact_id=$inviter->contact_id;
				$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
				$invite_user->save();
				Yii::app()->user->setFlash('addnew',UserModule::t("Thank you! Your contact is successfully added!."));
				$this->redirect( array('addnew'));
			}
			else
			{
				$command=Yii::app()->db->createCommand();
				$command->select('contact_id,deleted');
				$command->from('tbl_invitedusers');
				$command->where("inviter_id=:inviter and contact_id=:contact",array(":inviter"=>Yii::app()->user->id,":contact"=>$contact_id));
				$datareader=$command->query();
				$dr=$datareader->read();
				if($dr!=true)
				{
					$invite_user= new Invitedusers;
					$invite_user->inviter_id=Yii::app()->user->id;
					$invite_user->contact_id=$contact_id;
					$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
					$invite_user->save();
					Yii::app()->user->setFlash('addnew',UserModule::t("Thank you! Your contact is successfully added!."));
				}
				else if($dr['deleted']==1){
						$command=Yii::app()->db->createCommand();
						$inviter=Yii::app()->user->id;
	//$command->delete('tbl_invitedusers','inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$_GET['id']));
						$command->update('tbl_invitedusers',array('deleted'=>0),'inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$dr['contact_id']));
						$datareader=$command->execute();
				}
				else{
					Yii::app()->user->setFlash('addnew',UserModule::t("The Contact is already present.Please add another contact."));
				}
			}
			
			}
			else if(isset($_POST['yt0'])){
			
				$command=Yii::app()->db->createCommand();
				$command->select('contact_id');
				$command->from('tbl_invites');
				$command->where("email=:email",array(":email"=>$_POST['Invite']['email']));
				$datareader=$command->query();
				$dr=$datareader->read();$contact_id=$dr['contact_id'];
				$inviter= new Invite;
				if($dr!=true){
					$inviter->first_name=$_POST['Invite']['first_name'];
					$inviter->last_name=$_POST['Invite']['last_name'];
					$inviter->dob=$_POST['dob'];
					$inviter->contact_name=$_POST['Invite']['first_name']." ".$_POST['Invite']['last_name'];
					$inviter->work_phone=$_POST['Invite']['work_phone'];
					$inviter->home=$_POST['Invite']['home'];
					$inviter->mobile_phone=$_POST['Invite']['mobile_phone'];
					$inviter->fax=$_POST['Invite']['fax'];
					$inviter->email=$_POST['Invite']['email'];
					$inviter->company=$_POST['Invite']['company'];
					$inviter->save();
					$invite_user= new Invitedusers;
					$invite_user->inviter_id=Yii::app()->user->id;
					$invite_user->contact_id=$inviter->contact_id;
					$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
					$invite_user->save();
					Yii::app()->user->setFlash('addnew',UserModule::t("Thank you! Your contact is successfully added!."));
					$this->redirect(array('index'));
				}
				else
				{
					$command=Yii::app()->db->createCommand();
					$command->select('contact_id,deleted');
					$command->from('tbl_invitedusers');
					$command->where("inviter_id=:inviter and contact_id=:contact",array(":inviter"=>Yii::app()->user->id,":contact"=>$contact_id));
					$datareader=$command->query();
					$dr=$datareader->read();
					if($dr!=true)
					{
						$invite_user= new Invitedusers;
						$invite_user->inviter_id=Yii::app()->user->id;
						$invite_user->contact_id=$contact_id;
						$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
						$invite_user->save();
						Yii::app()->user->setFlash('index',UserModule::t("Thank you! Your contact is successfully added!."));
					}
					else if($dr['deleted']==1){
							$command=Yii::app()->db->createCommand();
							$inviter=Yii::app()->user->id;
							$command->update('tbl_invitedusers',array('deleted'=>0),'inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$inviter,":contact"=>$dr['contact_id']));
							$datareader=$command->execute();
					}
					else{
						Yii::app()->user->setFlash('addnew',UserModule::t("The Contact is already present.Please add another contact."));
					}

				}
			}
		}
		else
		{
			Yii::app()->user->setFlash('addnew',UserModule::t("The Contact is already present.Please add another contact."));
		}
		    // form inputs are valid, do something here
		   // return;
		
	    }
    	$this->render('addnew',array('model'=>$model));
	
	}

	public function actionGet_contacts($provider_box,$type)
	{
	
		foreach ($_POST as $key=>$val)
			if (strpos($key,'check_')!==false)
				$selected_contacts[$_POST['email_'.$val]]=$_POST['name_'.$val];
			elseif (strpos($key,'email_')!==false)
			{
				$temp=explode('_',$key);$counter=$temp[1];
			if (is_numeric($temp[1])) $contacts[$val]=$_POST['name_'.$temp[1]];
			}
		if(isset($_POST['step'])){
		$step=$_POST['step'];
		if($step='import')
		{
			if(isset($selected_contacts)){
			foreach($selected_contacts as $key=>$value)
			{	
				$key_arr=$key;
				$val_arr=$value;
				$command=Yii::app()->db->createCommand();
				$command->select('contact_id');
				$command->from('tbl_invites');
				$command->where("email=:email",array(":email"=>$key_arr));
				$datareader=$command->query();
				$dr=$datareader->read();
				$contact_id=$dr['contact_id'];
				if($dr!=true)
				{
					$inviter= new Invite;	
					$inviter->contact_name=$val_arr;
					$inviter->email=$key_arr;
					$inviter->save();
					$invite_user= new Invitedusers;
					$invite_user->inviter_id=Yii::app()->user->id;
					$invite_user->email_service=$provider_box;
					$invite_user->contact_id=$inviter->contact_id;
					$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
					$invite_user->save();
				}
				else
				{
					$command=Yii::app()->db->createCommand();
					$command->select('contact_id');
					$command->from('tbl_invitedusers');
					$command->where("inviter_id=:inviter and contact_id=:contact",array(":inviter"=>Yii::app()->user->id,":contact"=>$contact_id));
					$datareader=$command->query();
					if(($dr=$datareader->read())!=true)
					{
						$invite_user= new Invitedusers;
						$invite_user->inviter_id=Yii::app()->user->id;
						$invite_user->email_service=$provider_box;
						$invite_user->contact_id=$contact_id;
						$invite_user->subscription_key=UserModule::encrypting(microtime().$inviter->email);
						$invite_user->save();

						
					}
				}

			}
			$this->redirect( array('index'));
		}		
		}
		}
		$this->render('get_contacts',array('provider_box'=>$provider_box));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	/*public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
*/
	public function loadModel($id)
	{
		$model=Invite::model()->findByPk($id);
		if($model===null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
/*
	protected function performAjaxValidation($model)
	{
	    if(isset($_POST['ajax']) && $_POST['ajax']==='account-form')
	    {
		echo CActiveForm::validate($model);
		Yii::app()->end();
	    }
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
