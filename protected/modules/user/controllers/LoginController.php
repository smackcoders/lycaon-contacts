<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			$profile=new Profile;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				 $model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
					if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false){
						$command=Yii::app()->db->createCommand();
						$command->select('*');
						$command->from('tbl_users');
						$command->where("id=:id",array(":id"=>yii::app()->user->id));
						$datareader=$command->query();
						$drU=$datareader->readAll();
						$countU=0;
						$countP=0;
						$countT=0;
						foreach($drU as $keyU=>$valueU)
							{
							foreach($valueU as $key1U=>$value1U)	{
									$countT++;
								if($valueU[$key1U] == '')						
								{
									$countU++;
								}	
							}
						}
						$command->reset();
							
						$command->select('*');
						$command->from('tbl_profiles');
						$command->where("user_id=:user_id",array(":user_id"=>yii::app()->user->id));
						$datareader=$command->query();
						$drP=$datareader->readAll();
						foreach($drP as $keyP=>$valueP )
							{
							foreach($valueP as $key1P=>$value1P){
										$countT++;
									if($valueP[$key1P] == '')
									{
										$countP++;
									}	
								}				
								
							}
							$total= (($countP + $countU) / $countT) * 100;
							$_SESSION['tofill']=$total;
						$this->redirect(Yii::app()->controller->module->returnUrl);
						}
					else
												
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

}
