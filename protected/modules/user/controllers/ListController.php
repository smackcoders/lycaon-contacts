<?php

class ListController extends Controller
{
public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','addlist','group_contacts','members','postdata','delete','delete_list','managelist'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lists']))
		{
			$model->attributes=$_POST['Lists'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->list_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	*/

	public function actionView($id)
	{
		$this->render('members',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionManagelist()
	{
		$listid = $_REQUEST['id'];
		if(isset($_POST['adminusers'])){
			$command=Yii::app()->db->createCommand();
			$command->select('id');
			$command->from('tbl_users');
			$command->where("superuser=0 and adminuser=1");
			$datareader=$command->query();
			$dr1=$datareader->readAll();
			$command=Yii::app()->db->createCommand();
			$command->select('contact_id');
			$command->from('tbl_list_contacts');
			$command->where("list_id=:listid",array(":listid"=>$listid));
			$datareader=$command->query();
			$listcontacts=$datareader->readAll();//print('<pre>');
			foreach($dr1 as $adminuser)
			{//print_r($adminuser);
				$command=Yii::app()->db->createCommand();
				$command->select('*');
				$command->from('tbl_listadmins');
				$command->where("owner=:owner and list_id=:listid",array(":owner"=>$adminuser['id'],":listid"=>$listid));
				$datareader=$command->query();
				$dr2=$datareader->readAll();
				//print_r($dr2);
				# Check whether the admins are selected or not to the current list.
				if(in_array($adminuser['id'],$_POST['adminusers']))
				{

					#assigning the list contacts to the manager
					$command=Yii::app()->db->createCommand();
					$command->select('*');
					$command->from('tbl_invitedusers');
					$command->where("inviter_id=:inviter",array(":inviter"=>$adminuser['id']));
					$datareader=$command->query();
					$subscribers=$datareader->readAll();
					$sub = array();
					foreach($subscribers as $subs){
						$sub[]=$subs['contact_id'];
					}//print_r($sub);
					if(count($sub)!=0){
						foreach($listcontacts as $lcntcs){
							if(in_array($lcntcs['contact_id'],$sub)){
								$command=Yii::app()->db->createCommand();
								$command->update('tbl_invitedusers',array('deleted'=>0),'inviter_id=:inviter and contact_id=:contact', array(":inviter"=>$adminuser['id'],":contact"=>$lcntcs['contact_id']));
								$datareader=$command->execute();
							}
							else{
								$command=Yii::app()->db->createCommand();
								$command->select('email');
								$command->from('tbl_invites');
								$command->where("contact_id=:contact",array(":contact"=>$lcntcs['contact_id']));
								$datareader=$command->query();
								$find_mailid=$datareader->readAll();
								$invite_user= new Invitedusers;
								$invite_user->inviter_id=$adminuser['id'];
								$invite_user->contact_id=$lcntcs['contact_id'];
								$invite_user->subscription_key=UserModule::encrypting(microtime().$find_mailid[0]['email']);
								$invite_user->save();
							}
						}
					}
					else{
						foreach($listcontacts as $lcntcs){
							$command=Yii::app()->db->createCommand();
							$command->select('email');
							$command->from('tbl_invites');
							$command->where("contact_id=:contact",array(":contact"=>$lcntcs['contact_id']));
							$datareader=$command->query();
							$find_mailid=$datareader->readAll();
							$invite_user= new Invitedusers;
							$invite_user->inviter_id=$adminuser['id'];
							$invite_user->contact_id=$lcntcs['contact_id'];
							$invite_user->subscription_key=UserModule::encrypting(microtime().$find_mailid[0]['email']);
							$invite_user->save();
						}

					}//End of code to assign the list contacts to the admins
					
					foreach($dr2 as $array)
					{
						# Set the permission 1 if admin is selected and present in database for the specific list
						if(in_array($adminuser['id'],$array)){
							$model = List_admins::model()->findByPk($array['id']);
							$model->permission = 1;
							$model->save();
						}
					}
					# Insert if admin not present in the database
					if(empty($dr2))
					{
						$list= new List_admins;	
						$list->list_id=$listid;
						$list->owner=$adminuser['id'];
						$list->save();
					}
				}
				else{
					foreach($dr2 as $array)
					{
						# Set the permission 0 if admin is not selected and present in database for the specific list
						if(in_array($adminuser['id'],$array)){
							$model = List_admins::model()->findByPk($array['id']);
							$model->permission = 0;
							$model->save();
						}
					}
				}
			}

		} // End of condition to check the posted values
		$this->render('managelist');
	}

	public function actionMembers()
	{
		if(isset($_POST)){
		}
		if(isset($_GET['listpage']))
		{
		$list_page=$_GET['listpage']-1;
		$contact_page=$_GET['con_page']-1;
		}
		else
		{
		$list_page=0;
		$contact_page=0;

		$listid=$_REQUEST['id'];
		$command=Yii::app()->db->createCommand();
		$command->select('l.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_list_contacts l','c.contact_id=l.contact_id');
		$command->where("list_id=:listid",array(":listid"=>$listid));
		$datareader=$command->query();
		$dr=$datareader->readAll();

		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('c.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_invitedusers u','c.contact_id=u.contact_id');
		$command->where("inviter_id=:inviter and deleted=0",array(":inviter"=>$owner));
		$datareader=$command->query();
		$dr1=$datareader->readAll();
		Yii::app()->session['count_contacts'] = count($dr1);
		Yii::app()->session['count_contact_list'] = count($dr);
		}

		$listid=$_REQUEST['id'];
		$command=Yii::app()->db->createCommand();
		$command->select('l.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_list_contacts l','c.contact_id=l.contact_id');
		$command->where("list_id=:listid",array(":listid"=>$listid));
		$command->limit(10,$list_page*10);
		$datareader=$command->query();
		$dr=$datareader->readAll();

		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('c.contact_id,contact_name,email');
		$command->from('tbl_invites c');
		$command->join('tbl_invitedusers u','c.contact_id=u.contact_id');
		$command->where("inviter_id=:inviter and deleted=0",array(":inviter"=>$owner));
		$command->limit(10,$contact_page*10);
		$datareader=$command->query();
		$dr1=$datareader->readAll();
		$this->render('members',array('dr'=>$dr,'dr1'=>$dr1,'listpage'=>$list_page,'con_page'=>$contact_page,'owner'=>$owner,'id'=>$listid));
		
	}

	public function actionDelete()
	{
		$this->render('delete');
	}	
	
	public function actionDelete_list()
	{
		$this->render('delete_list');
	}	
	
	public function actionPostdata()
	{
		$this->render('postdata');
	}

	public function actionGroup_contacts()
	{
		$this->render('group_contacts');
	}

	public function actionAddlist()
	{
	    $model=new Lists;
	    // uncomment the following code to enable ajax-based validation
	    /*
	    if(isset($_POST['ajax']) && $_POST['ajax']==='lists-addlist-form')
	    {
		echo CActiveForm::validate($model);
		Yii::app()->end();
	    }
	    */
	    if(isset($_POST['ajax']) && $_POST['ajax']==='lists-addlist-form')
	    {
		echo UActiveForm::validate(array($model,$profile));
		Yii::app()->end();
            }
	    if(isset($_POST['Lists']))
	    {
		$model->attributes=$_POST['Lists'];
		if($model->validate())
		{
			
			foreach($_POST as $key=>$value)
			{
				$val=substr($key,0,3);	
				if($val =="ct_")
				{
					$contacts[]=$key;
				}
				else
				{
					//echo "U Got the output!";
				}
	
			}
			if(isset($contacts)){
				foreach($contacts as $cn){
					$cts[]=substr($cn,3);
				}
				//print_r($cts);
		   	// form inputs are valid, do something here
				$owner=Yii::app()->user->id;
				$list_name=$_REQUEST['Lists']['list_name'];
				$list_desc=$_REQUEST['Lists']['description'];
				$command=Yii::app()->db->createCommand();
				$command->select('list_name');
				$command->from('tbl_lists');
				$command->where("list_name=:l_name",array(":l_name"=>$list_name));
				$datareader=$command->query();
				$dr=$datareader->readAll();
				if($dr!=true)
				{
					$list= new Lists;	
					$list->list_name=$list_name;
					$list->description=$list_desc;
					$list->created_time=time();
					$list->owner_id=$owner;
					$list->save();
					$command=Yii::app()->db->createCommand();
					$command->select('list_id');
					$command->from('tbl_lists');
					$command->where("list_name=:listname",array(":listname"=>$_POST['Lists']['list_name']));
					$datareader=$command->query();
					$dr=$datareader->read();
					if(isset($dr['list_id'])){
						foreach($cts as $key=>$value){
							$list_contacts= new List_contacts;
							$list_contacts->list_id=$dr['list_id'];
							$list_contacts->contact_id=$value;
							$list_contacts->owner_id=$owner;
							$list_contacts->save();
						}
					}
					Yii::app()->user->setFlash('addlist',UserModule::t("Thank you! Your list is successfully created!."));
					$this->redirect(array('addlist'));
				}
				else
				{
			
					Yii::app()->user->setFlash('addlist',UserModule::t("The List name is already present.Please choose another list name!."));
				}
				Yii::app()->user->setFlash('addlist',UserModule::t("Thank you! Your list is successfully created!."));				
			}
			else{
				Yii::app()->user->setFlash('addlist',UserModule::t("Please select members to add the list!."));
			}
		    //return;
		}

		
	    }
			
	    $this->render('addlist',array('model'=>$model));
	} 

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Lists('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lists']))
			$model->attributes=$_GET['Lists'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Lists::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}*/

	/*public function loadUser($id=null)
	{
		if($this->_model===null)
		{
			if($id!==null || isset($_GET['id']))
				$this->_model=User::model()->findbyPk($id!==null ? $id : $_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}*/
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
