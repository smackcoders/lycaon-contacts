<?php

class RegistrationController extends Controller
{
	public $defaultAction = 'registration';


	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return (isset($_POST['ajax']) && $_POST['ajax']==='registration-form')?array():array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	/**
	 * Registration user
	 */
	public function actionRegistration() {
            $model = new RegistrationForm;
            //$profile=new Profile;
            //$profile->regMode = true;
	
		
	
            
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
			{
				echo UActiveForm::validate(array($model));//,$profile
				Yii::app()->end();
			}
			
		    if (Yii::app()->user->id) {
		    	$this->redirect(Yii::app()->controller->module->profileUrl);
		    } else {
		    	if(isset($_POST['RegistrationForm'])) {
					$model->attributes=$_POST['RegistrationForm'];
					//$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
					if($model->validate())//&&$profile->validate()
					{
						$soucePassword = $model->password;
						$model->activkey=UserModule::encrypting(microtime().$model->password);
						$model->password=UserModule::encrypting($model->password);
						$model->verifyPassword=UserModule::encrypting($_POST['verifypassword']);
						$model->createtime=time();
						$model->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
						$model->superuser=0;
						$model->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
								
						

							$command=Yii::app()->db->createCommand();
								$command->select('username');
								$command->from('tbl_users');
								$command->where("email=:email",array(":email"=>$_POST['RegistrationForm']['email']));
								$datareader=$command->query();
								$dr=$datareader->readAll();	
								if($dr){
									$errorEmail="User Already Exists";		
									$this->render('/user/registration',array('model'=>$model,'error1'=>$errorEmail));die;
									}


							//manual validation for password matching in password and verify password field
							else if(!($model->password==$model->verifyPassword)){
								$errorPassword='PassWord not matching';
								$this->render('/user/registration',array('model'=>$model,'error'=>$errorPassword));die;
								}
						else{
						if ($model->save()) {
							//$profile->user_id=$model->id;
							//$profile->save();
							$command=Yii::app()->db->createCommand();
							$command->select('*');
							$command->from('tbl_users');
							$command->order('id desc');
							$command->limit(1);
							$datareader=$command->query();
							$dr=$datareader->readAll();
							$command->insert('tbl_profiles', array("user_id"=>$dr[0]['id']));
							if (Yii::app()->controller->module->sendActivationMail) {
									
			$activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activkey" => $model->activkey, "email" => $model->email));
								$command=Yii::app()->db->createCommand();
								$command->select('*');
								$command->from('template');
								$command->where("description=:description",array(":description"=>'text'));
								$datareader=$command->query();
								$dr=$datareader->readAll();								
								$activation=stripslashes($dr[0]['content']); 
								$activation_temp=html_entity_decode($activation,ENT_QUOTES);
								echo($activation_temp);
						$header="Hello".' '.ucwords($model->username).'<br />';
						/*$greetings="Thank you for registering at LayconNewsletter".'<br />'.'<br />'; 	
						$message="Complete the Profile details after the activation ".'<br />';
						$fotter="Regards".'<br />'.'Smacker protocal'; */

								UserModule::sendMail($model->email,UserModule::t("Hi You registered from {site_name}",array('{site_name}'=>Yii::app()->name)),UserModule::t($header.'<br />'.$activation_temp." {activation_url}",array('{activation_url}'=>$activation_url)));
//activation mail cut from here
							}
							}
							if ((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin) {
									$identity=new UserIdentity($model->username,$soucePassword);
									$identity->authenticate();
									Yii::app()->user->login($identity,0);
									$this->redirect(Yii::app()->controller->module->returnUrl);
							} else {
								if (!Yii::app()->controller->module->activeAfterRegister&&!Yii::app()->controller->module->sendActivationMail) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
								} elseif(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please {{login}}.",array('{{login}}'=>CHtml::link(UserModule::t('Login'),Yii::app()->controller->module->loginUrl))));
								} elseif(Yii::app()->controller->module->loginNotActiv) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email or login."));
								} else {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email."));
								}
								$this->refresh();
							}
						}
					} else $this->render('/user/registration',array('model'=>$model));//,'profile'=>$profile//$profile->validate();
				}
			    $this->render('/user/registration',array('model'=>$model));//,'profile'=>$profile
		    }
	}
}
