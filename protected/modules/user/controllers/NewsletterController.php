<?php

class NewsletterController extends Controller
{

//	public $layout='//layouts/column2';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionPreview()
	{
		if(isset($_post)){
			$this->redirect(array('postdata'));
		}
		$this->render('preview');
	}

	public function actionUnsubscription()
	{
		$this->render('unsubscription');
	}

	public function actionSubscriber_profile()
	{
		$this->render('subscriber_profile');
	}

	public function actionPosteddata()
	{
		$this->render('posteddata');
	}

	public function actionDraftmessage()
	{
		$this->render('draftmessage');
	}

	public function actionResend()
	{
	    $model=new Savedmessages;

	    // uncomment the following code to enable ajax-based validation
	    /*
	    if(isset($_POST['ajax']) && $_POST['ajax']==='savedmessages-resend-form')
	    {
		echo CActiveForm::validate($model);
		Yii::app()->end();
	    }
	    */

	   /* if(isset($_POST['Savedmessages']))
	    {
		$model->attributes=$_POST['Savedmessages'];
		if($model->validate())
		{
		    // form inputs are valid, do something here
		    return;
		}
	    }*/
	    if(isset($_POST['yt0'])){
		$this->redirect( array('posteddata','id'=>$_POST));
	    }
	    $this->render('resend',array('model'=>$model));
	}

	public function actionDrafts()
	{
		$report=new Report;
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('message_id,subject,from_name,from_email,saved_at');
		$command->from('tbl_drafts');
		$command->where("saved_by=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();
		if(isset($_GET['page']))
		{
			$this->render('drafts',array('report'=>$report,'page'=>$_GET['page'],'dr'=>$dr));
		}
		else if(isset($_GET['next']))
		{
			$this->render('drafts',array('report'=>$report,'page'=>$_GET['next'],'dr'=>$dr));
		}
		else if(isset($_GET['prev']))
		{
			$this->render('drafts',array('report'=>$report,'page'=>$_GET['prev'],'dr'=>$dr));
		}
		else
		{
			$this->render('drafts',array('report'=>$report,'page'=>1,'dr'=>$dr));
		}
		//$this->render('drafts');
	}
		
	public function actionForward()
	{
		$this->render('forward');
	}
	
	public function actionView()
	{
		$report=new Report;
		if(isset($_POST['message'])){
			$this->redirect(array('forward','id'=>$_POST));
		}
		$this->render('view',array('report'=>$report));
	}

	public function actionReport()
	{
		$report=new Report;
		$owner=Yii::app()->user->id;
		$command=Yii::app()->db->createCommand();
		$command->select('report_id,subject,sender_name,sender_mail,scheduled_at,receiver_mail,status');
		$command->from('tbl_reports');
		$command->where("sender_id=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr=$datareader->readAll();
		if(isset($_GET['page']))
		{
			$this->render('report',array('report'=>$report,'page'=>$_GET['page'],'dr'=>$dr));
		}
		else if(isset($_GET['next']))
		{
			$this->render('report',array('report'=>$report,'page'=>$_GET['next'],'dr'=>$dr));
		}
		else if(isset($_GET['prev']))
		{
			$this->render('report',array('report'=>$report,'page'=>$_GET['prev'],'dr'=>$dr));
		}
		else
		{
			$this->render('report',array('report'=>$report,'page'=>1,'dr'=>$dr));
		}
	}
	
	public function actionSend_newsletter()
	{
		$model=new Newsletter;
		$owner=Yii::app()->user->id;
		if(isset($_POST['message'])){
			if(isset($_POST['type']))
			{
				if($_POST['type']=='template'){
					$command=Yii::app()->db->createCommand();
					$command->select('template_id,title,template');
					$command->from('tbl_template');
					$command->where("owner_id=:owner",array(":owner"=>$owner));
					$datareader=$command->query();
					$dr=$datareader->read();
				}
				if(isset($_POST['yt1'])){
					$this->redirect( array('posteddata','id'=>$_POST));
				}
				if(isset($_POST['yt0'])){
					$this->redirect( array('draftmessage','id'=>$_POST));
				}
			}
		}
		$command=Yii::app()->db->createCommand();
		$command->select('list_id');
		$command->from('tbl_listadmins');
		$command->where("owner=:owner",array(":owner"=>$owner));
		$datareader=$command->query();
		$dr_list=$datareader->readAll();
		if(count($dr_list)!=0){
			$this->render('send_newsletter',array('model'=>$model));
		}
		else{
			$this->render('index',array('model'=>$model));
		}
		
	}
	

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
