<?php

/**
 * This is the model class for table "tbl_invitedusers".
 *
 * The followings are the available columns in table 'tbl_invitedusers':
 * @property integer $id
 * @property integer $inviter_id
 * @property string $email_service
 * @property integer $contact_id
 * @property integer $deleted
 * @property integer $subscription
 * @property string $subscription_key
 *
 * The followings are the available model relations:
 * @property TblInvites $contact
 */
class Invitedusers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invitedusers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_invitedusers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inviter_id', 'required'),
			array('inviter_id, contact_id, deleted, subscription', 'numerical', 'integerOnly'=>true),
			array('email_service', 'length', 'max'=>20),
			array('subscription_key', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, inviter_id, email_service, contact_id, deleted, subscription, subscription_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contact' => array(self::BELONGS_TO, 'TblInvites', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'inviter_id' => 'Inviter',
			'email_service' => 'Email Service',
			'contact_id' => 'Contact',
			'deleted' => 'Deleted',
			'subscription' => 'Subscription',
			'subscription_key' => 'Subscription Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('inviter_id',$this->inviter_id);
		$criteria->compare('email_service',$this->email_service,true);
		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('subscription',$this->subscription);
		$criteria->compare('subscription_key',$this->subscription_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}