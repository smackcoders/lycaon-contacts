<?php

/**
 * This is the model class for table "tbl_invites".
 *
 * The followings are the available columns in table 'tbl_invites':
 * @property integer $contact_id
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property string $contact_name
 * @property string $work_phone
 * @property string $home
 * @property string $mobile_phone
 * @property string $fax
 * @property string $email
 * @property string $company
 * @property integer $status
 * @property integer $blacklist
 *
 * The followings are the available model relations:
 * @property TblInvitedusers[] $tblInvitedusers
 * @property TblListContacts[] $tblListContacts
 */
class Invite extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_invites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('status, blacklist', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, dob', 'length', 'max'=>30),
			array('contact_name', 'length', 'max'=>45),
			array('work_phone, home, mobile_phone, fax', 'length', 'max'=>20),
			array('email', 'length', 'max'=>128),
			array('company', 'length', 'max'=>125),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('contact_id, first_name, last_name, dob, contact_name, work_phone, home, mobile_phone, fax, email, company, status, blacklist', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblInvitedusers' => array(self::HAS_MANY, 'TblInvitedusers', 'contact_id'),
			'tblListContacts' => array(self::HAS_MANY, 'TblListContacts', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'contact_id' => 'Contact',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'dob' => 'Dob',
			'contact_name' => 'Contact Name',
			'work_phone' => 'Work Phone',
			'home' => 'Home',
			'mobile_phone' => 'Mobile Phone',
			'fax' => 'Fax',
			'email' => 'Email',
			'company' => 'Company',
			'status' => 'Status',
			'blacklist' => 'Blacklist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('contact_name',$this->contact_name,true);
		$criteria->compare('work_phone',$this->work_phone,true);
		$criteria->compare('home',$this->home,true);
		$criteria->compare('mobile_phone',$this->mobile_phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('blacklist',$this->blacklist);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}