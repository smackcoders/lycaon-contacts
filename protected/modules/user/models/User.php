<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $org_name
 * @property string $org_desc
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $contact_no
 * @property string $website
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $superuser
 * @property integer $adminuser
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property TblLists[] $tblLists
 * @property TblProfiles $tblProfiles
 * @property TblTemplate[] $tblTemplates
 */
class User extends CActiveRecord
{

	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANED=-1;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		//return 'tbl_users';
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return ((Yii::app()->getModule('user')->isAdmin())?array(
			array('username, password, email', 'required'),
			array('contact_no, createtime, lastvisit, superuser, adminuser, status', 'numerical', 'integerOnly'=>true),
			array('username, city, state, country', 'length', 'max'=>20),
			array('password, email, activkey', 'length', 'max'=>128),
			array('org_name', 'length', 'max'=>30),
			array('org_desc', 'length', 'max'=>50),
			array('website', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, email, org_name, org_desc, city, state, country, contact_no, website, activkey, createtime, lastvisit, superuser, adminuser, status', 'safe', 'on'=>'search'),
			):((Yii::app()->user->id==$this->id)?array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
		):array())
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$relations = array(
			'profile'=>array(self::HAS_ONE, 'Profile', 'user_id'),
		);
		if (isset(Yii::app()->getModule('user')->relations)) $relations = array_merge($relations,Yii::app()->getModule('user')->relations);
		return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' =>UserModule::t("ID"),
			'username' =>UserModule::t("Username"),
			'password' => UserModule::t("Password"),
			'email' => UserModule::t("Email"),
			'org_name' => UserModule::t("Org Name"),
			'org_desc' => UserModule::t("Org Desc"),
			'city' => UserModule::t("City"),
			'state' => UserModule::t("State"),
			'country' => UserModule::t("Country"),
			'contact_no' => UserModule::t("Contact No"),
			'website' => UserModule::t("Website"),
			'activkey' => UserModule::t("Activkey"),
			'createtime' => UserModule::t("Createtime"),
			'lastvisit' => UserModule::t("Lastvisit"),
			'superuser' => UserModule::t("Superuser"),
			'adminuser' => UserModule::t("Adminuser"),
			'status' => UserModule::t("Status"),
		);
	}
	public function scopes()
	    {
		return array(
		    'active'=>array(
		        'condition'=>'status='.self::STATUS_ACTIVE,
		    ),
		    'notactvie'=>array(
		        'condition'=>'status='.self::STATUS_NOACTIVE,
		    ),
		    'banned'=>array(
		        'condition'=>'status='.self::STATUS_BANED,
		    ),
		    'superuser'=>array(
		        'condition'=>'superuser=1',
		    ),
		    'notsafe'=>array(
		    	'select' => 'id, username, password, email, activkey, createtime, lastvisit, superuser, status',
		    ),
		);
	    }

	public function defaultScope()
	    {
		return array(
		    'select' => 'id, username, password, email, createtime, lastvisit, superuser, adminuser, status, org_name, org_desc, city, state, country, contact_no, website',
		);
	    }

	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('org_name',$this->org_name,true);
		$criteria->compare('org_desc',$this->org_desc,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('contact_no',$this->contact_no);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('activkey',$this->activkey,true);
		$criteria->compare('createtime',$this->createtime);
		$criteria->compare('lastvisit',$this->lastvisit);
		$criteria->compare('superuser',$this->superuser);
		$criteria->compare('adminuser',$this->adminuser);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
