<?php

/**
 * This is the model class for table "tbl_invites".
 *
 * The followings are the available columns in table 'tbl_invites':
 * @property integer $contact_id
 * @property string $contact_name
 * @property string $email
 * @property string $email_service
 * @property integer $blacklist
 */
class Invite extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_invites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('blacklist', 'numerical', 'integerOnly'=>true),
			array('contact_name', 'length', 'max'=>45),
			array('email', 'length', 'max'=>128),
			array('email_service', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('contact_id, contact_name, email, email_service, blacklist', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'contact_id' => 'Contact',
			'contact_name' => 'Contact Name',
			'email' => 'Email',
			'email_service' => 'Email Service',
			'blacklist' => 'Blacklist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('contact_name',$this->contact_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email_service',$this->email_service,true);
		$criteria->compare('blacklist',$this->blacklist);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}