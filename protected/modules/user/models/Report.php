<?php

/**
 * This is the model class for table "tbl_reports".
 *
 * The followings are the available columns in table 'tbl_reports':
 * @property integer $report_id
 * @property string $subject
 * @property integer $sender_id
 * @property string $sender_name
 * @property string $sender_mail
 * @property string $newsletter
 * @property string $scheduled_at
 * @property string $receiver_mail
 * @property integer $status
 * @property string $status_key
 */
class Report extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Report the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_reports';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, sender_id, sender_name, sender_mail, receiver_mail', 'required'),
			array('sender_id, status', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>255),
			array('sender_name, sender_mail, receiver_mail', 'length', 'max'=>125),
			array('scheduled_at', 'length', 'max'=>60),
			array('status_key', 'length', 'max'=>120),
			array('newsletter', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('report_id, subject, sender_id, sender_name, sender_mail, newsletter, scheduled_at, receiver_mail, status, status_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'report_id' => 'Report',
			'subject' => 'Subject',
			'sender_id' => 'Sender',
			'sender_name' => 'Sender Name',
			'sender_mail' => 'Sender Mail',
			'newsletter' => 'Newsletter',
			'scheduled_at' => 'Scheduled At',
			'receiver_mail' => 'Receiver Mail',
			'status' => 'Status',
			'status_key' => 'Status Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('report_id',$this->report_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('sender_id',$this->sender_id);
		$criteria->compare('sender_name',$this->sender_name,true);
		$criteria->compare('sender_mail',$this->sender_mail,true);
		$criteria->compare('newsletter',$this->newsletter,true);
		$criteria->compare('scheduled_at',$this->scheduled_at,true);
		$criteria->compare('receiver_mail',$this->receiver_mail,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('status_key',$this->status_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}