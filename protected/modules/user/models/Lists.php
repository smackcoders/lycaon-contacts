<?php

/**
 * This is the model class for table "tbl_lists".
 *
 * The followings are the available columns in table 'tbl_lists':
 * @property integer $list_id
 * @property string $list_name
 * @property string $description
 * @property integer $created_time
 * @property integer $owner_id
 *
 * The followings are the available model relations:
 * @property TblListContacts[] $tblListContacts
 * @property TblUsers $owner
 */
class Lists extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Lists the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_lists';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('list_name, owner_id', 'required'),
			array('created_time, owner_id', 'numerical', 'integerOnly'=>true),
			array('list_name', 'length', 'max'=>60),
			array('description', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('list_id, list_name, description, created_time, owner_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblListContacts' => array(self::HAS_MANY, 'TblListContacts', 'list_id'),
			'owner' => array(self::BELONGS_TO, 'TblUsers', 'owner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'list_id' => 'List',
			'list_name' => 'List Name',
			'description' => 'Description',
			'created_time' => 'Created Time',
			'owner_id' => 'Owner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('list_id',$this->list_id);
		$criteria->compare('list_name',$this->list_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_time',$this->created_time);
		$criteria->compare('owner_id',$this->owner_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
