<?php

/**
 * This is the model class for table "tbl_list_contacts".
 *
 * The followings are the available columns in table 'tbl_list_contacts':
 * @property integer $id
 * @property integer $list_id
 * @property integer $contact_id
 * @property integer $owner_id
 * @property integer $subscription
 * @property string $subscription_key
 *
 * The followings are the available model relations:
 * @property TblLists $list
 * @property TblInvites $contact
 */
class List_contacts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return List_contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_list_contacts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('list_id, contact_id, owner_id, subscription_key', 'required'),
			array('list_id, contact_id, owner_id, subscription', 'numerical', 'integerOnly'=>true),
			array('subscription_key', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, list_id, contact_id, owner_id, subscription, subscription_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'list' => array(self::BELONGS_TO, 'TblLists', 'list_id'),
			'contact' => array(self::BELONGS_TO, 'TblInvites', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'list_id' => 'List',
			'contact_id' => 'Contact',
			'owner_id' => 'Owner',
			'subscription' => 'Subscription',
			'subscription_key' => 'Subscription Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('list_id',$this->list_id);
		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('owner_id',$this->owner_id);
		$criteria->compare('subscription',$this->subscription);
		$criteria->compare('subscription_key',$this->subscription_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
