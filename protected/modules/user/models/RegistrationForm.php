<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $org_name
 * @property string $org_desc
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $contact_no
 * @property string $website
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $superuser
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property TblLists[] $tblLists
 * @property TblProfiles $tblProfiles
 * @property TblTemplate[] $tblTemplates
 */
class RegistrationForm extends CActiveRecord
{
	public $verifyPassword;
	public $verifyCode;	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RegistrationForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email', 'required'),
			array('createtime, lastvisit, superuser, status', 'numerical', 'integerOnly'=>true),
			array('username, contact_no', 'length', 'max'=>20),
			array('password, org_name, org_desc, city', 'length', 'max'=>75),
			array('email, activkey', 'length', 'max'=>128),
			array('state, country', 'length', 'max'=>25),
			array('website', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, email, org_name, org_desc, city, state, country, contact_no, website, activkey, createtime, lastvisit, superuser, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblLists' => array(self::HAS_MANY, 'TblLists', 'owner_id'),
			//'tblProfiles' => array(self::HAS_ONE, 'TblProfiles', 'user_id'),
			'tblTemplates' => array(self::HAS_MANY, 'TblTemplate', 'owner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'org_name' => 'Org Name',
			'org_desc' => 'Org Desc',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'contact_no' => 'Contact No',
			'website' => 'Website',
			'activkey' => 'Activkey',
			'createtime' => 'Createtime',
			'lastvisit' => 'Lastvisit',
			'superuser' => 'Superuser',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('org_name',$this->org_name,true);
		$criteria->compare('org_desc',$this->org_desc,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('contact_no',$this->contact_no,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('activkey',$this->activkey,true);
		$criteria->compare('createtime',$this->createtime);
		$criteria->compare('lastvisit',$this->lastvisit);
		$criteria->compare('superuser',$this->superuser);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
