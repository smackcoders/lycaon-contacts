<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <tt><?php echo __FILE__; ?></tt></li>
	<li>Layout file: <tt><?php echo $this->getLayoutFile('main'); ?></tt></li>
</ul>
<pre>
 <?php 
 /*$message = new YiiMailMessage;
 $message->setBody('Message content here with HTML', 'text/html');
 $message->subject = 'My Subject';
 $message->addTo('fredrickm@smackcoders.com');
 $message->from = Yii::app()->params['adminEmail'];
 Yii::app()->mail->send($message);*/
$this->redirect(array('/user/login'));
?>
</pre>
<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
