<?php
$this->breadcrumbs=array(
	'Invites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Invite', 'url'=>array('index')),
	array('label'=>'Manage Invite', 'url'=>array('admin')),
);
?>

<h1>Create Invite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>