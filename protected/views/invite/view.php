<?php
$this->breadcrumbs=array(
	'Invites'=>array('index'),
	$model->contact_id,
);

$this->menu=array(
	array('label'=>'List Invite', 'url'=>array('index')),
	array('label'=>'Create Invite', 'url'=>array('create')),
	array('label'=>'Update Invite', 'url'=>array('update', 'id'=>$model->contact_id)),
	array('label'=>'Delete Invite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->contact_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invite', 'url'=>array('admin')),
);
?>

<h1>View Invite #<?php echo $model->contact_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'contact_id',
		'first_name',
		'last_name',
		'dob',
		'contact_name',
		'work_phone',
		'home',
		'mobile_phone',
		'fax',
		'anniversary',
		'email',
		'status',
		'blacklist',
	),
)); ?>
