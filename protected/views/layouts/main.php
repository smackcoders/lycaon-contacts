<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<script type= "text/javascript" src = "<?php echo Yii::app()->request->baseUrl; ?>/protected/modules/user/views/asset/js/jquery-ui.min.js"></script>
	<script type= "text/javascript" src = "<?php echo Yii::app()->request->baseUrl; ?>/protected/modules/user/views/asset/js/countries3.js"></script>
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/comman.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/_samples/sample.js" type="text/javascript"></script>
	<link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript">
		function makeDisable(){
		    var x=document.getElementById("template")
		    x.disabled=true
		}
		function makeEnable(){
		    var x=document.getElementById("template")
		    x.disabled=false
		}
		function Test(rad){
		 var rads=document.getElementsByName(rad.name);
		 document.getElementById('editor').style.display=(rads[1].checked)?'none':'block';
		 document.getElementById('editor').style.display=(rads[2].checked)?'block':'none';

		}
	</script>

	<script language="javascript"> 
	function toggle() {
		var ele = document.getElementById("toggleText");
		var text = document.getElementById("displayText");
		if(ele.style.display == "block") {
	    		ele.style.display = "none";
			text.innerHTML = "Create List";
	  	}
		else {
			ele.style.display = "block";
			text.innerHTML = "Create List";
		}
	} 
	</script>
</head>

<body>
<div id="header_background"></div> 
<div id="menu_background"></div> 

<div class="container" id="page">

	<div id="header">
		<div id="logo"></div>
	</div><!-- header -->
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/user/user/about')),
				array('label'=>'Contact', 'url'=>array('/user/invite/index')),
				array('label'=>'Newsletter', 'url'=>array('/user/newsletter/index')),
				array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
<div class="bounce"></div>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
