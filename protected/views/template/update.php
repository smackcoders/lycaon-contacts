<?php
$this->breadcrumbs=array(
	'Templates'=>array('index'),
	$model->title=>array('view','id'=>$model->template_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Template', 'url'=>array('index')),
	array('label'=>'Create Template', 'url'=>array('create')),
	array('label'=>'View Template', 'url'=>array('view', 'id'=>$model->template_id)),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<h1>Update Template <?php echo $model->template_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>