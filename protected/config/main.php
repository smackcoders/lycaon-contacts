<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
require_once('config.php');
require_once('mail_conf.php');
global $ly_config;
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Lycaon Contacts',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'ext.yii-mail.YiiMailMessage',
		'ext.editMe.helpers.ExtEditMeHelper',
		'application.extensions.yii-mail.*',
	),

	'modules'=>array(
		'user'=>array(
		  'tableUsers' => 'tbl_users',
		  'tableProfiles' => 'tbl_profiles',
		  'tableProfileFields' => 'tbl_profiles_fields',
		),
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'@dm!n',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl' => array('/user/login'),
		),

		'mail' => array(
			  'class' => 'ext.yii-mail.YiiMail',
			  'transportType' => 'smtp', // change to 'php' when running in real domain.
			  'viewPath' => 'application.views.mail',
			  'logging' => true,
			  'dryRun' => false,
			  'transportOptions' => array(
			  'host' => $ly_config["mail_host"],  //if not work, try smtp.googlemail.com
			  'username' => $ly_config["mail_username"],
			  'password' => $ly_config["mail_password"],
			  'port' => $ly_config["port_no"],
			  'encryption' => $ly_config["enc"],
		  	),
		),

		// uncomment the following to enable URLs in path-format
		
		/*'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),*/
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host='.$ly_config["host"].';dbname='.$ly_config["db_name"],
			'emulatePrepare' => true,
			'username' => $ly_config["db_username"],
			'password' => $ly_config["db_password"],
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
		
	//'theme'=>'normal',
);
