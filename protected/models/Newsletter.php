<?php

/**
 * This is the model class for table "tbl_newsletter".
 *
 * The followings are the available columns in table 'tbl_newsletter':
 * @property integer $message_id
 * @property string $subject
 * @property string $from_name
 * @property string $from_email
 * @property integer $type
 * @property string $message
 * @property integer $template_id
 * @property string $scheduled_at
 * @property integer $list_id
 *
 * The followings are the available model relations:
 * @property TblTemplate $template
 * @property TblLists $list
 */
class Newsletter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Newsletter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_newsletter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, from_name, from_email, type', 'required'),
			array('type, template_id, list_id', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>255),
			array('from_name, from_email', 'length', 'max'=>125),
			array('scheduled_at', 'length', 'max'=>60),
			array('message', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('message_id, subject, from_name, from_email, type, message, template_id, scheduled_at, list_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'template' => array(self::BELONGS_TO, 'TblTemplate', 'template_id'),
			'list' => array(self::BELONGS_TO, 'TblLists', 'list_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message_id' => 'Message',
			'subject' => 'Subject',
			'from_name' => 'From Name',
			'from_email' => 'From Email',
			'type' => 'Type',
			'message' => 'Message',
			'template_id' => 'Template',
			'scheduled_at' => 'Scheduled At',
			'list_id' => 'List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('from_name',$this->from_name,true);
		$criteria->compare('from_email',$this->from_email,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('scheduled_at',$this->scheduled_at,true);
		$criteria->compare('list_id',$this->list_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}