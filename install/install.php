<html>
<head>
<title>Lycaon Contacts-Installation</title>
<link rel="stylesheet" type="text/css" href="../css/comman.css" />
<link rel="stylesheet" type="text/css" href="../css/main.css" />
<script type="text/javascript">
function notempty(){
	var host= document.getElementById('host').value.length;
	var dbname= document.getElementById('dbname').value.length;
	var uname= document.getElementById('uname').value.length;
	var admin_uname= document.getElementById('admin_uname').value.length;
	var admin_pwd= document.getElementById('admin_pwd').value.length;
	var admin_mail= document.getElementById('admin_mail').value.length;
	if((host == 0)||(dbname == 0)||(uname == 0)||(admin_uname == 0)||(admin_pwd == 0)||(admin_mail == 0)){
		if(host == 0){
			document.getElementById('host').style.borderColor="red";
			alert("Please enter the Hostname");
			setTimeout(function() {document.config.host.focus()} ,10);
		}
		else if(dbname == 0){
			document.getElementById('dbname').style.borderColor="red";
			alert("Please enter the Databasename");
			setTimeout(function() {document.config.dbname.focus()} ,10);
		}
		else if(uname == 0){
			document.getElementById('uname').style.borderColor="red";
			alert("Please enter the Username");
			setTimeout(function() {document.config.uname.focus()} ,10);
		}
		else if(admin_uname == 0){
			document.getElementById('admin_uname').style.borderColor="red";
			alert("Please enter your Admin Username");
			setTimeout(function() {document.config.admin_uname.focus()} ,10);
		}
		else if(admin_pwd == 0){
			document.getElementById('admin_pwd').style.borderColor="red";
			alert("Please enter your Admin Password");
			setTimeout(function() {document.config.admin_pwd.focus()} ,10);
		}
		else if(admin_mail == 0){
			document.getElementById('admin_mail').style.borderColor="red";
			alert("Please enter your Admin Password");
			setTimeout(function() {document.config.admin_mail.focus()} ,10);
		}

		return false;
	}
	else{
		return true;
	}
}
</script>
</head>
<body>
<div class="box">
	<form method="post" name="config">
		<div class="install">
			<div id="logo"></div>
			<table>
				<tr>
					<td colspan="2" style="text-align:center;">Database Configuration</td>
				</tr>
				<tr>
					<td>
						<label>Host Name <em>*</em></label>
					</td>
					<td>
						<input type="text" name="host" id="host"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Database Name <em>*</em></label>
					</td>
					<td>
						<input type="text" name="dbname" id="dbname"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Username <em>*</em></label>
					</td>
					<td>
						<input type="text" name="uname" id="uname"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Password</label>
					</td>
					<td>
						<input type="text" name="pwd" id="pwd"/>
					</td>
				</tr>	
				<tr>
					<td colspan="2" style="text-align:center;">Admin Configuration</td>
				</tr>
				<tr>
					<td>
						<label>Admin Username <em>*</em></label>
					</td>
					<td>
						<input type="text" name="admin_uname" id="admin_uname"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Admin Password <em>*</em></label>
					</td>
					<td>
						<input type="text" name="admin_pwd" id="admin_pwd"/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Admin E-Mail <em>*</em></label>
					</td>
					<td>
						<input type="text" name="admin_mail" id="admin_mail"/>
					</td>
				</tr>	
			</table>
				<input type="submit" value="Install" onClick="return notempty()" style="margin-left:45%;" class="uiButton"/>
		</div>
	</form>
</div>
</body>
</html>
<?php
$string='<?php global $ly_config;$ly_config["host"] ="' .$_REQUEST['host']. '";$ly_config["db_name"] ="' .$_REQUEST['dbname']. '";$ly_config["db_username"] ="' .$_REQUEST['uname']. '";$ly_config["db_password"] ="' .$_REQUEST['pwd']. '";$ly_config["admin_username"] ="' .$_REQUEST['admin_uname']. '";$ly_config["admin_password"] ="' .$_REQUEST['admin_pwd']. '";$ly_config["admin_mail"] ="' .$_REQUEST['admin_mail']. '";?>';

	$fp = fopen("../protected/config/config.php", "w");

	fwrite($fp, $string);

	fclose($fp);
	require_once('CreateTables.inc.php');
?>
