<?php
require_once(dirname(__FILE__)."/protected/config/config.php");
if(!empty($ly_config["host"]) && !empty( $ly_config["db_name"]) && !empty($ly_config["db_username"]) && !empty($ly_config["db_password"])){
	// change the following paths if necessary
	$yii=dirname(__FILE__).'/framework/yii.php';
	$config=dirname(__FILE__).'/protected/config/main.php';
	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
	require_once($yii);
	Yii::createWebApplication($config)->run();
}
else{
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	$location= explode ('/',$pageURL);
	$loc .=$location[0].'/'.$location[1];
	echo($loc);
	header("Location: http://".$loc."/install/install.php");
}
?>
